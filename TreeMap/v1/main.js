var svg = d3.select("svg"),
	width = +svg.attr("width"),
	height = +svg.attr("height");

var fader = color => d3.interpolateRgb(color, "#fff")(0.2),
	color = d3.scaleOrdinal(d3.schemeCategory10.map(fader)),
	format = d3.format(",d");

var treemap = d3
	.treemap()
	.tile(d3.treemapResquarify)
	.size([width, height])
	.round(true)
	.paddingInner(1);

d3.json("data.json").then(data => {
	var root = d3
		.hierarchy(data)
		.eachBefore(d => {
			d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name;
		})
		.sum(sumBySize)
		.sort((a, b) => b.height - a.height || b.value - a.value);

	treemap(root);

	var cell = svg
		.selectAll("g")
		.data(root.leaves())
		.enter()
		.append("g")
		.attr("transform", d => "translate(" + d.x0 + "," + d.y0 + ")");

	cell
		.append("rect")
		.attr("id", d => d.data.id)
		.attr("width", d => d.x1 - d.x0)
		.attr("height", d => d.y1 - d.y0)
		.attr("fill", d => color(d.parent.data.id));

	cell
		.append("clipPath")
		.attr("id", d => "clip-" + d.data.id)
		.append("use")
		.attr("xlink:href", d => "#" + d.data.id);

	cell
		.append("text")
		.attr("clip-path", d => "url(#clip-" + d.data.id + ")")
		.selectAll("tspan")
		.data(d => d.data.name.split(/(?=[A-Z][^A-Z])/g))
		.enter()
		.append("tspan")
		.attr("x", 4)
		.attr("y", (d, i) => 13 + i * 10)
		.text(d => d);

	cell.append("title").text(d => d.data.id + "\n" + format(d.value));

	d3
		.selectAll("input")
		.data([sumBySize, sumByCount], function(d) {
			return d ? d.name : this.value;
		})
		.on("change", changed);

	var timeout = d3.timeout(function() {
		d3
			.select('input[value="sumByCount"]')
			.property("checked", true)
			.dispatch("change");
	}, 2000);

	function changed(sum) {
		timeout.stop();
		treemap(root.sum(sum));
		cell
			.transition()
			.duration(750)
			.attr("transform", d => "translate(" + d.x0 + "," + d.y0 + ")")
			.select("rect")
			.attr("width", d => d.x1 - d.x0)
			.attr("height", d => d.y1 - d.y0);
	}
});

function sumByCount(d) {
	return d.children ? 0 : 1;
}

function sumBySize(d) {
	return d.size;
}
