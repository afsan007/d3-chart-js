var data = [
	{ x: 0, y: 0 },
	{ x: 1, y: 3 },
	{ x: 2, y: 12 },
	{ x: 3, y: 8 },
	{ x: 4, y: 17 },
	{ x: 5, y: 15 },
	{ x: 6, y: 10 },
	{ x: 7, y: 5 }
];

var xScale = d3
	.scaleLinear()
	.domain([0, 7])
	.range([25, 175]);
var yScale = d3
	.scaleLinear()
	.domain([0, 20])
	.range([175, 25]);

var line = d3
	.line()
	// .curve(d3.curveLinearClosed)
	// .curve(d3.curveLinear)
	// .curve(d3.curveStep)
	// .curve(d3.curveStepBefore)
	// .curve(d3.curveBasisClosed)
	// .curve(d3.curveBundle.beta(0.1))
	// .curve(d3.curveCardinal.tension(1))
	// .curve(d3.curveCatmullRom.alpha(0.2))
	.curve(d3.curveMonotoneX)
	.x(d => xScale(d.x))
	.y(d => yScale(d.y));

d3
	.select("#demo1a")
	.datum(data)
	.append("path")
	.attr("d", d => line(d))
	.attr("stroke", "black")
	.attr("fill", "none");
