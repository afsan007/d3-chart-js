// blockbuilder.org / mbostock / 7607535;
var svg = d3.select('svg'),
	margin = 20,
	diameter = +svg.attr('width'),
	g = svg
		.append('g')
		.attr('transform', 'translate(' + diameter / 2 + ',' + diameter / 2 + ')')

var color = d3
	.scaleLinear()
	.domain([-1, 5])
	.range(['hsl(152,80%,80%)', 'hsl(228,30%,40%)'])
	.interpolate(d3.interpolateHcl)

var pack = d3
	.pack()
	.size([diameter - margin, diameter - margin])
	.padding(2)

d3.json('data.json').then(root => {
	root = d3
		.hierarchy(root)
		.sum(d => d.size)
		.sort((a, b) => b.value - a.value)

	var focus = root,
		nodes = pack(root).descendants(),
		view

	// Reset figure to normal size
	svg.on('click', () => zoom(root))

	var circle = g
		.selectAll('circle')
		.data(nodes)
		.enter()
		.append('circle')
		.attr('class', d =>
			d.parent ? (d.children ? 'node' : 'node node--leaf') : 'node node--root'
		)
		.style('fill', d => (d.children ? color(d.depth) : null))
		.on('click', d => {
			console.log(focus === d)
			if (focus !== d) zoom(d), d3.event.stopPropagation()
		})

	var text = g
		.selectAll('text')
		.data(nodes)
		.enter()
		.append('text')
		.attr('class', 'label')
		.style('fill-opacity', d => (d.parent === root ? 1 : 0))
		.style('display', d => (d.parent === root ? 'inline' : 'none'))
		.text(d => d.data.name)

	var node = g.selectAll('circle,text')

	zoomTo([root.x, root.y, root.r * 2 + margin])

	function zoom(d) {
		var focus0 = focus
		focus = d

		var transition = d3
			.transition()
			.duration(d3.event.altKey ? 7500 : 750)
			.tween('zoom', function (d) {
				var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin])
				return t => zoomTo(i(t))
			})
		//review this part
		transition
			.selectAll('text')
			.filter(function (d) {
				return d.parent === focus || this.style.display === 'inline'
			})
			.style('fill-opacity', d => (d.parent === focus ? 1 : 0))
			.on('start', function (d) {
				if (d.parent === focus) this.style.display = 'inline'
			})
			.on('end', function (d) {
				if (d.parent !== focus) this.style.display = 'none'
			})
	}

	function zoomTo(v) {
		var k = diameter / v[2]
		view = v
		node.attr(
			'transform',
			d => 'translate(' + (d.x - v[0]) * k + ',' + (d.y - v[1]) * k + ')'
		)
		circle.attr('r', d => d.r * k)
	}
})
