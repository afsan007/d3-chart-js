// Set tooltips
var tip = d3
	.tip()
	.attr("class", "d3-tip")
	.offset([-10, 0])
	.html(d => {
		return d;
	});

var svg = d3.select("svg"),
	width = +svg.attr("width"),
	height = +svg.attr("height");

var unemployment = d3.map();

var path = d3.geoPath();

var x = d3
	.scaleLinear()
	.domain([1, 10])
	.rangeRound([600, 860]);

var color = d3
	.scaleThreshold()
	.domain(d3.range(2, 10))
	.range(d3.schemeBlues[9]);
var g = svg
	.append("g")
	.attr("class", "key")
	.attr("transform", "translate(0,40)");

g.selectAll("rect")
	.data(
		color.range().map(d => {
			d = color.invertExtent(d);
			if (d[0] == null) d[0] = x.domain()[0];
			if (d[1] == null) d[1] = x.domain()[1];
			return d;
		})
	)
	.enter()
	.append("rect")
	.attr("height", 8)
	.attr("x", d => x(d[0]))
	.attr("width", d => x(d[1]) - x(d[0]))
	.attr("fill", d => color(d[0]));

g.append("text")
	.attr("class", "caption")
	.attr("x", x.range()[0])
	.attr("y", -6)
	.attr("fill", "#000")
	.attr("text-anchor", "start")
	.attr("font-weight", "bold")
	.text("Unemployment rate");

g.call(
	d3
		.axisBottom(x)
		.tickSize(13)
		.tickFormat((x, i) => (i ? x : x + "%"))
		.tickValues(color.domain())
)
	.select(".domain")
	.remove();

Promise.all([
	d3.json("data/us-map.json"),
	d3.tsv("data/map.tsv", d => unemployment.set(d.id, +d.rate))
])
	.then(data => {
		data = data[0];
		let countries = topojson.feature(data, data.objects.counties).features;
		let cities = topojson.mesh(data, data.objects.states, (a, b) => a !== b);
		svg
			.append("g")
			.attr("class", "counties")
			.selectAll("path")
			.data(countries)
			.enter()
			.append("path")
			.attr("fill", d => color((d.rate = unemployment.get(d.id))))
			.attr("d", path)
			.call(tip)
			.on("mouseover", d => tip.show(d))
			.on("mouseout", d => tip.hide(d))
			.append("title")
			.text(d => d.rate + "%");
		svg
			.append("path")
			.datum(cities)
			.attr("class", "states")
			.attr("d", path);
	})
	.catch(console.log);

function ready(us) {}
