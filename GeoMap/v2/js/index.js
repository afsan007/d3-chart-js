var format = d3.format(",");
// "https://gist.githubusercontent.com/mbostock/4090846/raw/d534aba169207548a8a3d670c9c2cc719ff05c47/world-110m.json";

// Set tooltips
var tip = d3
	.tip()
	.attr("class", "d3-tip")
	.offset([-10, 0])
	.html(
		d =>
			"<strong>Country: </strong><span class='details'>" +
			d.properties.name +
			"<br></span>" +
			"<strong>Population: </strong><span class='details'>" +
			format(d.population) +
			"</span>"
	);

var margin = { top: 0, right: 0, bottom: 0, left: 0 },
	width = 980 - margin.left - margin.right,
	height = 700 - margin.top - margin.bottom;

var color = d3
	.scaleThreshold()
	.domain([
		10000,
		100000,
		500000,
		1000000,
		5000000,
		10000000,
		50000000,
		100000000,
		500000000,
		1500000000
	])
	.range([
		"rgb(247,251,255)",
		"rgb(222,235,247)",
		"rgb(198,219,239)",
		"rgb(158,202,225)",
		"rgb(107,174,214)",
		"rgb(66,146,198)",
		"rgb(33,113,181)",
		"rgb(8,81,156)",
		"rgb(8,48,107)",
		"rgb(3,19,43)"
	]);

// var path = d3.geoPath();

var projection = d3
	// .geoMercator()
	.geoOrthographic()
	.scale(300)
	.translate([width / 2, height / 1.5])
	.rotate([10, 20]);

var path = d3.geoPath().projection(projection);
var drag = d3
	.drag()
	.on("start", function() {
		var proj = projection.rotate();
		m0 = [d3.event.sourceEvent.pageX, d3.event.sourceEvent.pageY];
		o0 = [-proj[0], -proj[1]];
	})
	.on("drag", function() {
		if (m0) {
			var m1 = [d3.event.sourceEvent.pageX, d3.event.sourceEvent.pageY],
				o1 = [o0[0] + (m0[0] - m1[0]) / 4, o0[1] + (m1[1] - m0[1]) / 4];
			projection.rotate([-o1[0], -o1[1]]);
		}

		// Update the map
		path = d3.geoPath().projection(projection);
		d3.selectAll("path").attr("d", path);
		d3
			.selectAll("circle")
			.attr("cx", d => projection([d.long, d.lat])[0])
			.attr("cy", d => projection([d.long, d.lat])[1]);
	});

var svg = d3
	.select("body")
	.append("svg")
	.attr("width", width)
	.attr("height", height)
	.call(drag)
	.append("g")
	.attr("class", "map");

svg.call(tip);

function mouseover(d) {
	tip.show(d);
	d3
		.select(this)
		.style("opacity", 1)
		.style("stroke", "white")
		.style("stroke-width", 3);
}
function mouseout(d) {
	tip.hide(d);
	d3
		.select(this)
		.style("opacity", 0.8)
		.style("stroke", "white")
		.style("stroke-width", 0.3);
}
var graticule = d3.geoGraticule();
var markers = [
	{ long: 9.083, lat: 42.149 }, // corsica
	{ long: 7.26, lat: 43.71 }, // nice
	{ long: 2.349, lat: 48.864 }, // Paris
	{ long: -1.397, lat: 43.664 }, // Hossegor
	{ long: 3.075, lat: 50.64 }, // Lille
	{ long: -3.83, lat: 58 } // Morlaix
];
Promise.all([
	d3.json("data/world_countries.json"),
	d3.tsv("data/world_population.tsv")
]).then(([data, population]) => {
	var populationById = {};
	population.forEach(d => (populationById[d.id] = +d.population));
	data.features.forEach(d => (d.population = populationById[d.id]));

	svg
		.append("g")
		.attr("class", "countries")
		.selectAll("path")
		.data(data.features)
		.enter()
		.append("path")
		.attr("d", path)
		.style("fill", d => color(populationById[d.id]))
		.style("stroke", "white")
		.style("stroke-width", 1.5)
		.style("opacity", 0.8)
		.style("stroke", "white")
		.style("stroke-width", 0.3)
		.on("mouseover", mouseover)
		.on("mouseout", mouseout);
	svg
		.append("path")
		.datum(graticule)
		.attr("class", "graticule")
		.attr("d", path);
	svg
		.selectAll("myCircles")
		.data(markers)
		.enter()
		.append("circle")
		.attr("cx", d => projection([d.long, d.lat])[0])
		.attr("cy", d => projection([d.long, d.lat])[1])
		.attr("r", 4)
		.style("fill", "69b3a2")
		.attr("stroke", "#69b3a2")
		.attr("stroke-width", 3)
		.attr("fill-opacity", 0.4);
});
