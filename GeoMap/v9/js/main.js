var tip = d3
	.tip()
	.attr("class", "d3-tip")
	.offset([-10, 0])
	.html(d => {
		return '<p class="tip">' + d.name + "</p>";
	});
var width = 950,
	height = 700;

var colors = {
	clickable: "#313436;",
	hover: "#2626",
	clicked: "red",
	clickhover: "darkred",
};

var projection = d3
	.geoOrthographic()
	.scale(300)
	.translate([width / 2, height / 2])
	.clipAngle(90)
	.precision(10);

var path = d3.geoPath().projection(projection);

var graticule = d3.geoGraticule();

var map = d3
	.select("body")
	.append("svg")
	.attr("width", width)
	.attr("height", height)
	.attr("class", "map");

map
	.append("defs")
	.append("path")
	.datum({ type: "Sphere" })
	.attr("id", "sphere")
	.attr("d", path);

map.append("use").attr("xlink:href", "#sphere");

map.append("path").datum(graticule).attr("class", "graticule").attr("d", path);

var format = d3.format(",");

Promise.all([
	d3.json("data/world-110m.json"),
	d3.tsv("data/world-country-names.tsv"),
]).then(([world, names]) => {
	let countries = topojson.feature(world, world.objects.countries).features;
	countries = countries
		.filter(d =>
			names.some(n => {
				if (d.id == n.id) return (d.name = n.name);
			})
		)
		.sort((a, b) => a.name.localeCompare(b.name));

	map
		.insert("path", ".graticule")
		.datum(topojson.feature(world, world.objects.land))
		.attr("class", "land");

	for (i = 0; i < names.length; i++) {
		for (j = 0; j < countries.length; j++) {
			if (countries[j].id == names[i].id) {
				map
					.insert("path", ".graticule")
					.datum(countries[j])
					.attr("fill", colors.clickable)
					.attr("d", path)
					.attr("class", "clickable")
					.attr("data-country-id", j)
					.attr("d", path)
					.call(tip)
					.on("click", function () {
						// clear all clicked classes

						d3
							.selectAll(".clicked")
							.classed("clicked", false)
							.attr("fill", colors.clickable);
						// sign this element with clicked class
						d3.select(this).classed("clicked", true).attr("fill", colors.clicked);

						// rotate map to new position
						d3
							.select(".clicked")
							.transition()
							.duration(1250)
							.tween("rotate", function () {
								// console.log(countries[d3.select(this).attr("data-country-id")]);
								let p = d3.geoCentroid(
									countries[d3.select(this).attr("data-country-id")]
								);
								let p1 = d3.geoCentroid(
									countries[d3.select(this)._groups[0][0].__data__]
								);
								// let r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
								let r = d3.geoInterpolate(projection.rotate(), [-p[0], -p[1]]);
								return t => {
									projection.rotate(r(t));
									map.selectAll("path").attr("d", path);
								};
							});
					})
					.on("mouseover", d => tip.show(d))
					.on("mousemove", function (d) {
						var c = d3.select(this);
						c.attr("fill", c.classed("clicked") ? colors.clickhover : colors.hover);
					})
					.on("mouseout", function (d) {
						var c = d3.select(this);
						c.attr("fill", c.classed("clicked") ? colors.clicked : colors.clickable);
						tip.hide(d);
					});
			}
		}
	}

	map
		.insert("path", ".graticule")
		.datum(topojson.mesh(world, world.objects.countries, (a, b) => a !== b))
		.attr("class", "boundary")
		.attr("d", path);
});

d3.select(self.frameElement).style("height", height + "px");
