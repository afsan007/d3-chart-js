var width = 900,
	height = 800;

var projection = d3
	.geoOrthographic()
	.scale(220)
	.translate([width / 2, height / 2])
	.precision(0.1);

var path = d3.geoPath().projection(projection);

var graticule = d3.geoGraticule();
var drag = d3
	.drag()
	.on("start", function() {
		// Adapted from http://mbostock.github.io/d3/talk/20111018/azimuthal.html and updated for d3 v3
		var proj = projection.rotate();
		m0 = [d3.event.sourceEvent.pageX, d3.event.sourceEvent.pageY];
		o0 = [-proj[0], -proj[1]];
	})
	.on("drag", function() {
		if (m0) {
			var m1 = [d3.event.sourceEvent.pageX, d3.event.sourceEvent.pageY],
				o1 = [o0[0] + (m0[0] - m1[0]) / 4, o0[1] + (m1[1] - m0[1]) / 4];
			projection.rotate([-o1[0], -o1[1]]);
		}

		// Update the map
		path = d3.geoPath().projection(projection);
		d3.selectAll("path").attr("d", path);
	});

var zoom = d3
	.zoom()
	.scaleExtent([1, 8])
	.on("zoom", () => {
		d3.selectAll("path").attr("transform", d3.event.transform);
	});
var svg = d3
	.select("body")
	.append("svg")
	.attr("width", width)
	.attr("height", height)
	.call(drag)
	.call(zoom);

d3.json("data/world-110m.json").then(world => {
	console.log(world);
	console.log(topojson.feature(world, world.objects.land));
	console.log(world.objects.countries);

	svg
		.append("path")
		.datum(topojson.feature(world, world.objects.land))
		.attr("class", "land")
		.attr("d", path);

	svg
		.append("path")
		.datum(topojson.mesh(world, world.objects.countries, (a, b) => a !== b))
		.attr("class", "boundary")
		.attr("d", path);

	svg
		.append("path")
		.datum(graticule)
		.attr("class", "graticule")
		.attr("d", path);
});

d3.select(self.frameElement).style("height", height + "px");
