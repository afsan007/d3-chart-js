new (class worldTour {
	constructor(props) {
		this.element = props.element;
		this._vars();
		this.Scaffold();
		this.GeoFunc();
		this.FetchData_Run();
	}

	_vars() {
		this.oceanColor = "steelblue";
		this.landColor = "#262626";
		this.flyingArcColor = "tomato";
		this.flyingArcShadowColor = "#333";
		this.flyingArcShadowStrokeColor = "#ccc";
		this.cityMarkerColor = "#999";
		this.cityLabelColor = "#666";
		this.cityLabelShadowColor = "#eee";

		this.flyingArcWidth = 2;
		this.flyingArcShadowWidth = 0.5;
		this.flyingArcShadowOpacity = 0.5;
		this.flyingArcShadowBlur = 5;
		this.cityMarkerRadius = 2;
		this.cityLabelFont = '3rem "Montserrat", sans-serif';
		this.cityLabelTextAlign = "center";
		this.cityLabelOffset = [0, -7];
		this.cityLabelShadowBlur = 0;
		this.loftiness = 1.3;
		this.transitionDuration = 4000;
		this.transitionEase = d3.easeQuad;
		this.link;
		this.focalPoint;
		this.flyingArcLength;
		this.links = [];
		this.linksMap = d3.map();
	}

	_run() {
		this.shuffle();
	}

	Scaffold() {
		this.width = 960;
		this.height = 600;
		this.canvas = d3
			.select(this.element)
			.append("canvas")
			.attr("width", this.width)
			.attr("height", this.height);
		this.context = this.canvas.node().getContext("2d");
		this.context.font = this.cityLabelFont;
		this.context.font;
		this.context.textAlign = this.cityLabelTextAlign;
	}
	FetchData_Run() {
		Promise.all([d3.json("data/world.json"), d3.json("data/capitals.json")]).then(
			([world, capitals]) => {
				this.world = world;
				this.capitals = capitals;
				this.BuildMap();
				this.Utility();
				this._run();
			}
		);
	}
	GeoFunc() {
		this.projection = d3
			.geoOrthographic()
			.scale((this.height - 10) / 2)
			.translate([this.width / 2, this.height / 2])
			.precision(0.1);

		this.loftedProjection = d3
			.geoOrthographic()
			.scale(((this.height - 10) / 2) * this.loftiness)
			.translate([this.width / 3, this.height / 2])
			.precision(0.1);

		this.path = d3.geoPath().projection(this.projection).context(this.context);
		this.swoosh = d3
			.line()
			.curve(d3.curveNatural)
			// .defined(d => this.projection.invert(d))
			.context(this.context);
	}

	Utility() {
		this.locationAlongArc = (start, end, theta) =>
			d3.geoInterpolate(start, end)(theta);
		this.focusGlobeOnPoint = point => {
			let x = point[0];
			let y = point[1];
			let cx = x;
			let cy = y - 25;
			let rotation = [-cx, -cy];
			this.projection.rotate(rotation);
			this.loftedProjection.rotate(rotation);
		};

		this.lineLength = points => {
			let d = 0;
			for (let i = 0; i < points.length - 1; i++) {
				let x0 = points[0][0];
				let y0 = points[0][1];
				let x1 = points[1][0];
				let y1 = points[1][1];
				let dx = x1 - x0;
				let dy = y1 - y0;
				d += Math.sqrt(dx * dx + dy * dy);
			}
			return d;
		};

		this.randomInt = n => Math.floor(Math.random() * n);

		this.pluckRandom = array => {
			var n = array.length - 1,
				i = this.randomInt(n);
			return array[i];
		};

		this.flyingArc = link => {
			let source = link.source;
			let target = link.target;
			let middle = this.locationAlongArc(source, target, 0.5);
			return [
				this.projection(source),
				this.loftedProjection(middle),
				this.projection(target),
			];
		};
		this.link = this.pluckRandom(this.links);
		this.shuffle = () => {
			this.focalPoint = d3.geoInterpolate(this.link.source, this.link.target);
			this.flyingArcLength = this.lineLength(this.flyingArc(this.link));

			let tick = elapsed => {
				let t0 = elapsed / this.transitionDuration;
				let t = this.transitionEase(t0);
				this.Draw(t);
				if (t0 >= 1) {
					timer.stop();

					// The current target becomes the next source. Pick the next
					// target at random.
					var targetLinks = this.linksMap.get(this.link.targetId);
					this.link = this.pluckRandom(targetLinks);

					this.shuffle();
				}
			};
			let timer = d3.timer(tick);
		};
	}

	BuildMap() {
		this.sphere = { type: "Sphere" };
		this.land = topojson.feature(this.world, this.world.objects.land);

		// Add unique ID for each capital city
		this.capitals.features.forEach((d, i) => (d.id = i));

		// Spawn links between capital city locations
		this.capitals.features.forEach(a =>
			this.capitals.features.forEach(b => {
				// Don't want a city to link to itself
				if (a !== b) {
					var source = a.geometry.coordinates,
						target = b.geometry.coordinates;

					// Build GeoJSON feature from this link
					var feature = {
						type: "Feature",
						geometry: {
							type: "LineString",
							coordinates: [source, target],
						},
						properties: {
							sourceName: a.properties.name,
							targetName: b.properties.name,
							sourceId: a.id,
							targetId: b.id,
						},
					};

					// Two restrictions:
					// 1) Don't link cities that are too close together
					// 2) Don't link cities that are too far apart
					// TODO: Figure out clipping and remove restriction (2)
					var length = d3.geoLength(feature),
						minLength = Math.PI / 6,
						maxLength = Math.PI / 2;
					if (length > minLength && length < maxLength) {
						this.links.push({
							sourceId: a.id,
							targetId: b.id,
							source: source,
							target: target,
							feature: feature,
						});
					}
				}
			})
		);
		this.linksMap = d3
			.nest()
			.key(d => d.sourceId)
			.map(this.links);
	}

	Draw(t) {
		this.context.clearRect(0, 0, this.width, this.height);
		// Rotate globe to focus on the flying arc
		this.focusGlobeOnPoint(this.focalPoint(t));

		// Oceans
		this.context.beginPath();
		this.path(this.sphere);
		this.context.fillStyle = this.oceanColor;
		this.context.fill();

		// Land
		this.context.beginPath();
		this.path(this.land);
		this.context.fillStyle = this.landColor;
		this.context.fill();

		// Flying arc
		this.context.beginPath();
		this.swoosh(this.flyingArc(this.link));
		this.context.setLineDash([t * this.flyingArcLength * 1.7, 1e6]);
		this.context.lineWidth = this.flyingArcWidth;
		this.context.strokeStyle = this.flyingArcColor;
		this.context.stroke();

		// Flying arc's shadow
		this.context.beginPath();
		this.path(this.link.feature);
		this.context.setLineDash([t * this.flyingArcLength * 1.6, 1e6]);
		this.context.globalAlpha = this.flyingArcShadowOpacity;
		this.context.shadowColor = this.flyingArcShadowColor;
		this.context.shadowBlur = this.flyingArcShadowBlur;
		this.context.lineWidth = this.flyingArcShadowWidth;
		this.context.strokeStyle = this.flyingArcShadowStrokeColor;
		this.context.stroke();
		this.context.shadowBlur = 0;
		this.context.globalAlpha = 1;

		//Source city marker
		let p = this.projection(this.link.source);
		let x = p[0];
		let y = p[1];
		this.context.beginPath();
		this.context.arc(x, y, this.cityMarkerRadius, 0, 2 * Math.PI);
		this.context.fillStyle = this.cityMarkerColor;
		this.context.fill();

		// Source city label
		x = x + this.cityLabelOffset[0];
		y = y + this.cityLabelOffset[1];
		this.context.shadowBlur = this.cityLabelShadowBlur;
		this.context.shadowColor = this.cityLabelShadowColor;
		this.context.fillStyle = this.cityLabelColor;
		this.context.fillText(this.link.feature.properties.sourceName, x, y);
		this.context.shadowBlur = 0;

		// Target city marker
		p = this.projection(this.link.target);
		x = p[0];
		y = p[1];
		this.context.beginPath();
		this.context.arc(x, y, this.cityMarkerRadius, 0, 2 * Math.PI);
		this.context.fillStyle = this.cityMarkerColor;
		this.context.fill();

		// Target city label
		x = x + this.cityLabelOffset[0];
		y = y + this.cityLabelOffset[1];
		this.context.shadowBlur = this.cityLabelShadowBlur;
		this.context.shadowColor = this.cityLabelShadowColor;
		this.context.fillStyle = this.cityLabelColor;
		this.context.fillText(this.link.feature.properties.targetName, x, y);
		this.context.shadowBlur = 0;
	}
})({ element: "#worldTour" });
