var width = 960,
	height = 800;

var projection = d3
	.geoOrthographic()
	.center([0, 5])
	.scale(200)
	.rotate([-180, 0]);

var svg = d3
	.select("body")
	.append("svg")
	.classed("container", true)
	.attr("width", width)
	.attr("height", height);

var path = d3.geoPath().projection(projection);
var graticule = d3.geoGraticule();

var g = svg.append("g");
g.append("defs")
	.append("path")
	.datum({ type: "Sphere" })
	.attr("id", "sphere")
	.attr("d", path);

g.append("use").attr("xlink:href", "#sphere");

var drag = d3
	.drag()
	.on("start", () => {
		var proj = projection.rotate();
		m0 = [d3.event.sourceEvent.pageX, d3.event.sourceEvent.pageY];
		o0 = [-proj[0], -proj[1]];
	})
	.on("drag", () => {
		if (m0) {
			var m1 = [d3.event.sourceEvent.pageX, d3.event.sourceEvent.pageY],
				o1 = [o0[0] + (m0[0] - m1[0]) / 4, o0[1] + (m1[1] - m0[1]) / 4];
			projection.rotate([-o1[0], -o1[1]]);
		}

		// Update the map
		path = d3.geoPath().projection(projection);
		g.selectAll("path").attr("d", path);
		svg.selectAll("path").attr("d", path);
	});

var zoom = d3
	.zoom()
	.scaleExtent([1, 8])
	.on("zoom", () => {
		g.selectAll("path").attr("transform", d3.event.transform);
		svg.selectAll("path").attr("transform", d3.event.transform);
	});

svg.call(drag);
svg.call(zoom);

d3.json("world.json").then(world => {
	var countries = topojson.feature(world, world.objects.countries).features,
		neighbors = topojson.neighbors(world.objects.countries.geometries);
	var color = d3.scaleOrdinal(d3.schemeDark2);
	// svg
	// 	.append("path")
	// 	.datum({ type: "Sphere" })
	// 	.attr("class", "sphere")
	// 	.attr("d", path);
	g.selectAll("path")
		.data(countries)
		.enter()
		.append("path")
		// .style("fill", (d, i) =>
		// 	color((d.color = (d3.max(neighbors[i], n => countries[n].color) + 1) | 0))
		// )
		.attr("class", "countries")
		.attr("d", path);

	g.append("path")
		.datum(topojson.mesh(world, world.objects.countries, (a, b) => a !== b))
		.attr("class", "boundary")
		.attr("d", path);
	// .style("fill", (d, i) =>
	// 	color((d.color = (d3.max(neighbors[i], n => countries[n].color) + 1) | 0))
	// );

	svg
		.append("path")
		.datum(graticule)
		.attr("class", "graticule")
		.attr("d", path);
});
