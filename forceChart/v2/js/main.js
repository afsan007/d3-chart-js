const simulationPod = d3ForcePod()(document.body);

/*********** EDIT ME **********/

simulationPod.genNodes({
	density: 0.00025, // nodes/px
	radiusRange: [1, 8], // px
	yRange: [0, 0], // px
	velocityRange: [0, 1], // px/tick
	velocityAngleRange: [90, 90], // 0=right, 90=down
});

simulationPod
	.addForce(d3.forceBounce().radius(d => d.r)) // bounce
	.addForce(
		d3
			.forceX()
			.x(window.innerWidth / 2)
			.strength(0.002)
	); // vertical

/******************************/

// d3ForcePod: https://github.com/vasturiano/d3-force-pod

// Get inspired at https://github.com/vasturiano/d3-force-registry
