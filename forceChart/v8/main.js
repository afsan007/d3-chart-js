//Constants for the SVG
var width = 500,
	height = 500;

//Set up the colour scale
var color = d3.scaleOrdinal(d3.schemeCategory10);

let simulation = d3
	.forceSimulation()
	.force(
		"link",
		d3
			.forceLink()
			.id(d => d.group)
			.distance(30)
	)
	.force("charge", d3.forceManyBody().strength(-120))
	.force("center", d3.forceCenter(this.width / 2, this.height / 2));

var svg = d3
	.select("body")
	.append("svg")
	.attr("width", width)
	.attr("height", height)
	.on("dblclick", threshold);

d3.json("data.json").then(graph => {
	console.log(graph);
	graphRec = JSON.parse(JSON.stringify(graph)); //Add this line

	//Creates the graph data structure out of the json data
	simulation.nodes(graph.nodes);
	simulation.force("link").links(graph.links);

	//Create all the line svgs but without locations yet
	var link = svg
		.selectAll(".link")
		.data(graph.links)
		.enter()
		.append("line")
		.attr("class", "link")
		.style("stroke-width", d => Math.sqrt(d.value));

	//Do the same with the circles for the nodes - no
	var node = svg
		.selectAll(".node")
		.data(graph.nodes)
		.enter()
		.append("circle")
		.attr("class", "node")
		.attr("r", 8)
		.style("fill", function(d) {
			return color(d.group);
		})
		.call(drag);

	//Now we are giving the SVGs co-ordinates - the force layout is generating the co-ordinates which this code is using to update the attributes of the SVG elements
	force.on("tick", function() {
		link
			.attr("x1", function(d) {
				return d.source.x;
			})
			.attr("y1", function(d) {
				return d.source.y;
			})
			.attr("x2", function(d) {
				return d.target.x;
			})
			.attr("y2", function(d) {
				return d.target.y;
			});

		node
			.attr("cx", function(d) {
				return d.x;
			})
			.attr("cy", function(d) {
				return d.y;
			});
	});

	//---Insert-------

	//adjust threshold

	function threshold(thresh) {
		graph.links.splice(0, graph.links.length);

		for (var i = 0; i < graphRec.links.length; i++) {
			if (graphRec.links[i].value > thresh) {
				graph.links.push(graphRec.links[i]);
			}
		}
		restart();
	}

	//Restart the visualisation after any node and link changes

	function restart() {
		link = link.data(graph.links);
		link.exit().remove();
		link
			.enter()
			.insert("line", ".node")
			.attr("class", "link");
		node = node.data(graph.nodes);
		node
			.enter()
			.insert("circle", ".cursor")
			.attr("class", "node")
			.attr("r", 5)
			.call(force.drag);
		force.start();
	}
	//---End Insert---
});
