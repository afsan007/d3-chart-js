new (class ForcesChart {
	constructor(props) {
		this.el = props.element
		this.wid = props.width
		this.heig = props.height
		this._run()
	}

	_run() {
		d3.json('data.json').then(data => {
			this.Scaffold()
			this.DataNormalize(data)
			this.Simulation()
			this.Draw()
		})
	}

	Scaffold() {
		let margin = { l: 10, r: 10, t: 10, b: 10 }

		this.width = this.wid - margin.l - margin.r
		this.height = this.heig - margin.b - margin.t

		this.svg = d3
			.select('#chart')
			.append('svg')
			.attr('width', this.width + margin.l + margin.r)
			.attr('height', this.height + margin.b + margin.t)
			.append('g')
			.attr('transform', `translate(${margin.l},${margin.t})`)
		this.nodesRadius = 10
	}

	Simulation() {
		this.forceSimulate = d3
			.forceSimulation()

			.force(
				'link',
				d3.forceLink().id(d => d.id)
			)
			.force('charge', d3.forceManyBody().strength(-100))
			.force('collide', d3.forceCollide().radius(this.nodesRadius))
			.force('center', d3.forceCenter(this.width / 2, this.height / 2))

		let ticked = () => {
			this.linkGraph
				.attr('x1', d => d.source.x)
				.attr('y1', d => d.source.y)
				.attr('x2', d => d.target.x)
				.attr('y2', d => d.target.y)

			this.nodeGraph.attr('cx', d => d.x).attr('cy', d => d.y)
		}

		return () => {
			this.forceSimulate.nodes(this.graph.nodes).on('tick', ticked)
			this.forceSimulate.force('link').links(this.graph.links)
		}
	}

	DataNormalize(data) {
		this.graph = { nodes: data.vertices, links: data.edges }

		this.graph.nodes.forEach((d, i) => {
			d.id = (i + 1).toString()
		})

		this.graph.links.forEach(d => {
			d.target = d.to.toString()
			d.source = d.from.toString()
		})
		this.graphRec = JSON.parse(JSON.stringify(this.graph)) //Add this line
	}

	Fisheye(forceSimulate, linkGraph) {
		var fisheye = d3.fisheye.circular().radius(200)
		this.svg.on('mousemove', function () {
			forceSimulate.stop()
			fisheye.focus(d3.mouse(this))
			d3
				.selectAll('circle')
				.each(d => (d.fisheye = fisheye(d)))
				.attr('cx', d => d.fisheye.x)
				.attr('cy', d => d.fisheye.y)
				.attr('r', d => d.fisheye.z * 8)
			linkGraph
				.attr('x1', d => d.source.fisheye.x)
				.attr('y1', d => d.source.fisheye.y)
				.attr('x2', d => d.target.fisheye.x)
				.attr('y2', d => d.target.fisheye.y)
		})
	}
	Highlight(nodeGraph, linkGraph) {
		//Toggle stores whether the highlighting is on
		var toggle = 0
		//Create an array logging what is connected to what
		var linkedByIndex = {}

		for (let i = 0; i < this.graph.nodes.length; i++) {
			linkedByIndex[i + ',' + i] = 1
		}
		this.graph.links.forEach(function (d) {
			linkedByIndex[d.source.index + ',' + d.target.index] = 1
		})
		//This function looks up whether a pair are neighbours
		function neighboring(a, b) {
			return linkedByIndex[a.index + ',' + b.index]
		}
		function connectedNodes() {
			if (toggle == 0) {
				//Reduce the opacity of all but the neighbouring nodes
				let d = d3.select(this).node().__data__
				nodeGraph.style('opacity', function (o) {
					return neighboring(d, o) | neighboring(o, d) ? 1 : 0.1
				})
				linkGraph.style('opacity', function (o) {
					return (d.index == o.source.index) | (d.index == o.target.index) ? 1 : 0.1
				})
				//Reduce the op
				toggle = 1
			} else {
				//Put them back to opacity=1
				nodeGraph.style('opacity', 1)
				linkGraph.style('opacity', 1)
				toggle = 0
			}
		}
		return connectedNodes
	}
	scales() {
		let colorize = d3.scaleOrdinal(d3.schemeCategory10)
		let weight = d3.scaleLinear().domain([0, 30]).range([0, 3])
		return { colorize, weight }
	}

	Draw() {
		const { colorize, weight } = this.scales()
		const { mouseout, mouseover } = this.mouseEvents(colorize)
		const simulate = this.Simulation()

		this.linkGraph = this.svg
			.append('g')
			.attr('class', 'links')
			.selectAll('line')
			.data(this.graph.links)
			.enter()
			.append('line')
			.attr('stroke-width', d => weight(d.weight))

		this.nodeGraph = this.svg
			.append('g')
			.attr('class', 'nodes')
			.selectAll('circle')
			.data(this.graph.nodes)
			.enter()
			.append('circle')
			.attr('r', this.nodesRadius)
			.attr('fill', d => colorize(d.Group))
			.on('mouseover', mouseover)
			.on('mouseout', mouseout)
			.on('dblclick', this.Highlight(this.nodeGraph, this.linkGraph))
			.call(this.DragFunc(this.forceSimulate))
		simulate()
		this.Fisheye(this.forceSimulate, this.linkGraph)
	}

	mouseEvents(colorize) {
		let mouseover = d => {
			let { id: vertex, Group: group } = d
			d3
				.select('#context')
				.text('v:' + vertex + ', group: ' + group)
				.style('color', colorize)
		}
		let mouseout = d => {
			d3.select('#context').text('')
		}
		return { mouseout, mouseover }
	}

	DragFunc(forceSimulate) {
		function dragStarted(d) {
			if (!d3.event.active) forceSimulate.alphaTarget(0.3).restart()
			d.fx = d.x
			d.fy = d.y
		}

		function dragged(d) {
			d.fx = d3.event.x
			d.fy = d3.event.y
		}

		function dragEnded(d) {
			if (!d3.event.active) forceSimulate.alphaTarget(0)
			d.fx = null
			d.fy = null
		}
		return d3
			.drag()
			.on('start', dragStarted)
			.on('end', dragEnded)
			.on('drag', dragged)
	}
})({ element: '#chart', width: 700, height: 600 })
