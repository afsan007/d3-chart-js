var svg = d3.select("svg"),
	width = +svg.attr("width"),
	height = +svg.attr("height");

var vSize = 5; // vertex size

// decide on a color scheme
var color = d3.scaleOrdinal(d3.schemeCategory10);
// list of colors based on colorbrewer instead

var weight = d3
	.scaleLinear()
	.domain([0, 30])
	.range([0, 3]);

// set up the simulation
// a force simulation with links, charge, and a force that keeps the graph centered
var simulation = d3
	.forceSimulation()
	.force(
		"link",
		d3.forceLink().id(d => d.id)
	)
	.force("charge", d3.forceManyBody().strength(-20))
	.force("center", d3.forceCenter(width / 2, height / 2));

d3.json("data.json").then(graph => {
	//--------- Data Fixes
	graph.nodes = graph.vertices;
	delete graph.vertices;

	graph.links = graph.edges;
	delete graph.edges;

	graph.nodes.forEach((d, i) => {
		d.id = (i + 1).toString();
		// 1 indexed source R
	});

	// fix source and to fields
	graph.links.forEach((d, i) => {
		d.source = d.from.toString();
		d.target = d.to.toString();
	});
	console.log("graph", graph);
	console.log("links", graph.links);
	console.log("nodes", graph.nodes);
	//-------------------------------------------------

	var link = svg
		.append("g")
		.attr("class", "links")
		.selectAll("line")
		.data(graph.links)
		.enter()
		.append("line")
		.attr("stroke-width", d => weight(d.weight));

	var node = svg
		.append("g")
		.attr("class", "nodes")
		.selectAll("circle")
		.data(graph.nodes)
		.enter()
		.append("circle")
		.attr("r", vSize)
		.attr("fill", d => color(d.Group))
		.on("mouseover", mouseover)
		.on("mouseout", mouseout)
		.call(drag());

	// set up simulation
	simulation.nodes(graph.nodes).on("tick", ticked);
	simulation.force("link").links(graph.links);
	console.log(graph);
	function ticked() {
		link
			.attr("x1", d => d.source.x)
			.attr("y1", d => d.source.y)
			.attr("x2", d => d.target.x)
			.attr("y2", d => d.target.y);

		node.attr("cx", d => d.x).attr("cy", d => d.y);
	}
});

//	Mouse Functions
function mouseout(d) {
	d3.select("#node").text("");
}
function mouseover(d) {
	let vertex = d.id; // counter, index
	let group = d.Group;
	let theColor = color(d.Group);

	d3
		.select("#node")
		.text("v: " + vertex + ", group: " + group)
		.style("color", theColor);
}

// Drag
function drag() {
	return d3
		.drag()
		.on("start", dragStarted)
		.on("drag", dragged)
		.on("end", dragEnded);
}
function dragStarted(d) {
	if (!d3.event.active) simulation.alphaTarget(0.3).restart();
	d.fx = d.x;
	d.fy = d.y;
}

function dragged(d) {
	d.fx = d3.event.x;
	d.fy = d3.event.y;
}

function dragEnded(d) {
	if (!d3.event.active) simulation.alphaTarget(0);
	d.fx = null;
	d.fy = null;
}
