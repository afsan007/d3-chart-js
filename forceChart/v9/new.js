new (class {
	constructor(props) {
		this.el = props.element
		this.width = props.width
		this.height = props.height
		this._run()
	}

	_run() {
		this.Scaffold()
		this.Scales()
		this.FetchData()
	}

	Scaffold() {
		let margin = { t: 10, b: 10, l: 10, r: 10 }
		this.svg = d3
			.select(this.el)
			.append('svg')
			.attr('width', this.width - margin.l - margin.r)
			.attr('height', this.height - margin.t - margin.b)
			.append('g')
			.attr(
				'transform',
				'translate(' + margin.l + ',' + margin.t + ')'
			)
	}

	Scales() {
		this.rScale = d3.scalePow().exponent(0.5).range([1, 50])
		this.cScale = d3.scaleOrdinal(d3.schemeCategory10)
		this.yScale = d3.scaleBand().range([50, this.height])
		this.xScale = d3.scaleBand().range([70, this.width])
	}

	FetchData() {
		const output = row => {
			if (row['2017'] === 0) return null
			row['2017'] = +row['2017'].replace(
				new RegExp(',', 'g'),
				''
			)
			return row
		}
		d3.csv('data.csv', output).then(data => {
			this.data = data
			this.DataNormalize()
			this.Simulation()
			this.Draw()
		})
	}

	DataNormalize() {
		var nestedData = d3
			.nest()
			.key(d => d['Grant/non-grant split'])
			.key(d => d['BEA Category'])
			.key(d => d['Agency Name'])
			// .key(function (d) { return d["Bureau Name"]; })
			.rollup(leaves => {
				return {
					length: leaves.length,
					budget: d3.sum(leaves, d => +d['2017']),
				}
			})
			.entries(this.data)
		console.log(nestedData)
		this.flatAgencies = []
		nestedData.forEach(g =>
			g.values.forEach(bea => {
				bea.values.forEach(a => {
					a.bea = bea.key
					a.grant = g.key
					this.flatAgencies.push(a)
				})
			})
		)
		console.log(this.flatAgencies)
		this.xScale.domain(nestedData[0].values.map(d => d.key))
		this.rScale.domain([
			0,
			d3.max(this.flatAgencies, d => d.value.budget),
		])
	}

	Simulation() {
		this.simulation = d3
			.forceSimulation(this.flatAgencies)
			// .forceSimulation()
			.force(
				'collide',
				d3
					.forceCollide()
					.radius(d => this.rScale(d.value.budget) + 1)
			)
			.force('x', d3.forceX(this.width / 2))
			.force('y', d3.forceY(this.height / 2))
	}

	Draw() {
		var cs = this.svg
			.selectAll('circle')
			.data(this.flatAgencies)
		var csEnter = cs
			.enter()
			.append('circle')
			.style('fill', d => this.cScale(d.bea))
			.attr('r', d => this.rScale(d.value.budget))

		csEnter
			.append('title')
			.text(d => d.key + ' $' + d.value.budget / 1000000 + 'M')

		this.svg
			.append('g')
			.attr('class', 'axis x')
			.style('display', 'none')
			.call(d3.axisTop(this.xScale))
			.attr('transform', 'translate(0,50)')

		d3.select('#chkBEA').on('change', () => {
			this.simulation.force(
				'x',
				d3.event.target.checked
					? d3
							.forceX(
								d =>
									this.xScale(d.bea) + this.xScale.bandwidth() / 2
							)
							.strength(0.3)
					: d3.forceX(this.width / 2)
			)
			this.svg
				.select('.x.axis')
				.style(
					'display',
					d3.event.target.checked ? 'block' : 'none'
				)
			this.simulation.alpha(0.3).restart()
		})

		d3.select('#chkGrant').on('change', () => {
			this.simulation.force(
				'x',
				d3.event.target.checked
					? d3
							.forceX(
								d =>
									this.xScale(d.bea) + this.xScale.bandwidth() / 2
							)
							.strength(0.3)
					: d3.forceX(this.width / 2)
			)

			this.svg
				.select('.x.axis')
				.style(
					'display',
					d3.event.target.checked ? 'block' : 'none'
				)
			this.simulation.alpha(0.3).restart()
		})
		this.simulation.on('tick', ticked)
		function ticked() {
			csEnter
				.merge(cs)
				.attr('cx', d => d.x)
				.attr('cy', d => d.y)
		}
	}
})({ element: '#budget', height: 400, width: 900 })
