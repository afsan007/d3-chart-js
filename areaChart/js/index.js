// Scaffold
let margin = { top: 20, right: 20, bottom: 30, left: 50 },
	width = 700 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;
let parseDate = d3.timeParse('%d-%b-%y');
// Initialize
let svg = d3
	.select('body')
	.append('svg')
	.attr('width', width + margin.left + margin.right)
	.attr('height', height + margin.top + margin.bottom)
	.append('g')
	.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

// Data Normalize
let x = d3.scaleTime().range([0, width]);
let y = d3.scaleLinear().range([height, 0]);
let area = d3
	.area()
	.x(d => x(d.date))
	.y1(d => y(d.close))
	.y0(y(0));

// Axises
let xAxisCall = d3.axisBottom();
let yAxisCall = d3.axisLeft();

let xAxis = svg
	.append('g')
	.attr('class', 'x axis')
	.attr('transform', 'translate(0,' + height + ')');
let yAxis = svg.append('g').attr('class', 'y axis');

//Labels
svg
	.append('text')
	.attr('transform', 'rotate(-90)')
	.attr('y', 6)
	.attr('dy', '.71em')
	.style('text-anchor', 'end')
	.text('Price ($)');

d3.tsv('data/data.tsv', d => {
	d.date = parseDate(d.date);
	d.close = +d.close;
	return d;
})
	.then(data => {
		console.log(data);
		x.domain(d3.extent(data, d => d.date));
		y.domain([0, d3.max(data, d => d.close)]);

		xAxis.call(xAxisCall.scale(x));
		yAxis.call(yAxisCall.scale(y));
		update(data);
		// Add area to chart
		// svg
		// 	.append('path')
		// 	.attr('class', 'area')
		// 	.attr('d', area(data));
		// svg
		// 	.selectAll('path')
		// 	.join('path')
		// 	.attr('class', 'area')
		// 	.attr('d', area(data));
	})
	.catch(console.log);

function update(data) {
	let t = d3.transition().duration(100);

	// ? Approach ONE
	// JOIN new data with old elements.
	let areas = svg.selectAll('path').data(data);

	// EXIT old elements not present in new data.
	areas
		.exit()
		.attr('class', 'exit')
		.remove();

	// ENTER new elements present in new data.
	areas
		.enter()
		.append('path')
		.attr('class', 'area')
		.attr('d', area(data));
}
