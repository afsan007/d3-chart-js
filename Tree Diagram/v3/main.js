var treeData = {
	name: "Top Level",
	children: [
		{
			name: "Level 2: A",
			children: [
				{
					name: "Son of A",
					children: [{ name: "Son of A" }, { name: "Daughter of A" }],
				},
				{ name: "Daughter of A" },
			],
		},
		{
			name: "Level 2: B",
			children: [{ name: "Son of A" }, { name: "Daughter of A" }],
		},
	],
};

// set the dimensions and margins of the diagram
var margin = { top: 40, right: 00, bottom: 50, left: 0 },
	width = 300 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

// declares a tree layout and assigns the size
var treemap = d3.tree().size([width, height]);

//  assigns the data to a hierarchy using parent-child relationships
var root = d3.hierarchy(treeData);

// maps the node data to the tree layout
root = treemap(root);

// append the svg object to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3
		.select("body")
		.append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom),
	g = svg
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
svg.call(
	d3.zoom().on("zoom", () => {
		g.attr("transform", d3.event.transform);
	})
);
// adds the links between the nodes
var link = g
	.selectAll(".link")
	.data(root.descendants().slice(1))
	.enter()
	.append("path")
	.attr("class", "link")
	.attr(
		"d",
		d =>
			"M" +
			d.x +
			"," +
			d.y +
			"C" +
			d.x +
			"," +
			(d.y + d.parent.y) / 2 +
			" " +
			d.parent.x +
			"," +
			(d.y + d.parent.y) / 2 +
			" " +
			d.parent.x +
			"," +
			d.parent.y
	);

let preNodes = g.selectAll(".node").data(root.descendants());

preNodes
	.exit()
	.attr("class", "exit")
	.remove();

// adds each node as a group
var node = preNodes
	.enter()
	.append("g")
	.attr("class", d => "node" + (d.children ? " node--internal" : " node--leaf"))
	.attr("transform", d => "translate(" + d.x + "," + d.y + ")");

// adds the circle to the node
node.append("circle").attr("r", 10);

// adds the text to the node
node
	.append("text")
	.attr("dy", ".5em")
	.attr("y", d => (d.height >= 2 ? -30 : -20))
	.style("text-anchor", "middle")
	.style("font-size", d => (d.height ? d.height * 0.75 : 0.5) + "rem")
	.merge(node)
	.transition()
	.duration(500)
	.attr("transform", d => (!d.children && width ? `rotate(${90})` : ""))
	.text(d => d.data.name);
