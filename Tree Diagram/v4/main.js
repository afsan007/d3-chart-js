var svg = d3.select("svg"),
	width = +svg.attr("width"),
	height = +svg.attr("height"),
	g = svg
		.append("g")
		.attr(
			"transform",
			"translate(" + (width / 2 + 40) + "," + (height / 2 + 90) + ")"
		);

var stratify = d3.stratify().parentId(function(d) {
	return d.id.substring(0, d.id.lastIndexOf("."));
});

var tree = d3
	.tree()
	.size([2 * Math.PI, 500])
	.separation(function(a, b) {
		return (a.parent == b.parent ? 1 : 2) / a.depth;
	});

const radialPoint = (x, y) => [
	(y = +y) * Math.cos((x -= Math.PI / 2)),
	y * Math.sin(x),
];

d3.csv("data.csv").then(data => {
	var root = tree(stratify(data));

	var link = g
		.selectAll(".link")
		.data(root.links())
		.enter()
		.append("path")
		.attr("class", "link")
		.attr(
			"d",
			d3
				.linkRadial()
				.angle(d => d.x)
				.radius(d => d.y)
		);

	let node = g.selectAll(".node").data(root.descendants());
	var nodeEnter = node
		.enter()
		.append("g")
		.attr("class", d => "node" + (d.children ? " node--internal" : " node--leaf"))
		.attr("transform", d => "translate(" + radialPoint(d.x, d.y) + ")");

	nodeEnter.append("circle").attr("r", 2.5);

	nodeEnter
		.append("text")
		.attr("dy", "0.31em")
		.attr("x", d => (d.x < Math.PI === !d.children ? 6 : -6))
		.attr("text-anchor", d => (d.x < Math.PI === !d.children ? "start" : "end"))
		.attr("transform", function(d) {
			return (
				"rotate(" +
				((d.x < Math.PI ? d.x - Math.PI / 2 : d.x + Math.PI / 2) * 180) / Math.PI +
				")"
			);
		})
		.text(d => d.id.substring(d.id.lastIndexOf(".") + 1));
});
