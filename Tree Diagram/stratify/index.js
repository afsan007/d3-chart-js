var stratify = d3
	.stratify()
	.id(d => d.id)
	.parentId(d => {
		console.log(
			"id:",
			d.id,
			"|| child:",
			d.id.split(".")[d.id.split(".").length - 1],
			"|| Parent:",
			d.id.substring(0, d.id.lastIndexOf("."))
		);
		return d.id.substring(0, d.id.lastIndexOf("."));
	});
let tool = d3.hierarchy({
	name: "root",
	children: [
		{ name: "child #1" },
		{
			name: "child #2",
			children: [
				{ name: "grandchild #1" },
				{ name: "grandchild #2" },
				{ name: "grandchild #3" },
			],
		},
	],
});
console.log(tool);
d3.csv("data.csv").then(data => {
	console.log(data);
	let root = stratify(data);
	console.log(root);
	console.log(root.ancestors());
});
