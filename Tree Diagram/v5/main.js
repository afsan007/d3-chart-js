var svg = d3.select("svg"),
	width = +svg.attr("width"),
	height = +svg.attr("height"),
	g = svg.append("g").attr("transform", "translate(40,0)");

var tree = d3.tree().size([height - 400, width - 160]);

var cluster = d3.cluster().size([height, width - 160]);

var stratify = d3.stratify().parentId(function(d) {
	return d.id.substring(0, d.id.lastIndexOf("."));
});
var t = d3.transition().duration(1000);
d3.csv("./flare.csv").then(data => {
	var root = stratify(data).sort(function(a, b) {
		return a.height - b.height || a.id.localeCompare(b.id);
	});

	root = tree(root);
	var link = g
		.selectAll(".link")
		.data(root.descendants().slice(1))
		.enter()
		.append("path")
		.attr("class", "link")
		.attr("d", diagonal);

	let nodeEnter = g
		.selectAll(".node")
		.data(root.descendants())
		.enter()
		.append("g");

	var node = nodeEnter
		.attr("class", d => "node" + (d.children ? " node--internal" : " node--leaf"))
		.style("opacity", 0)
		.attr("transform", d => "translate(" + d.y + "," + d.x + ")")
		.transition(t)
		.style("opacity", 1);

	nodeEnter.append("circle").attr("r", 2.5);

	nodeEnter
		.append("text")
		.transition(t)
		.attr("dy", 3)
		.attr("x", d => (d.children ? -8 : 8))
		.style("text-anchor", d => (d.children ? "end" : "start"))
		.text(d => d.id.substring(d.id.lastIndexOf(".") + 1));

	d3.selectAll("input").on("change", changed);

	// var timeout = setTimeout(function() {
	// 	d3
	// 		.select('input[value="tree"]')
	// 		.property("checked", true)
	// 		.dispatch("change");
	// }, 1000);
	function changed() {
		// timeout = clearTimeout(timeout);
		let chartType = this.value === "tree" ? tree : cluster;
		root = chartType(root);

		nodeEnter
			.transition(t)
			.attr("transform", d => "translate(" + d.y + "," + d.x + ")");
		link.transition(t).attr("d", diagonal);
	}
});

function diagonal(d) {
	return (
		"M" +
		d.y +
		"," +
		d.x +
		"C" +
		(d.parent.y + 100) +
		"," +
		d.x +
		" " +
		(d.parent.y + 100) +
		"," +
		d.parent.x +
		" " +
		d.parent.y +
		"," +
		d.parent.x
	);
}
