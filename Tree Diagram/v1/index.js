//--> USE STRATIFY METHOD

var svg = d3.select("svg"),
	width = +svg.attr("width"),
	height = +svg.attr("height"),
	g = svg.append("g").attr("transform", "translate(40,0)");

var tree = d3.tree().size([height, width - 160]);

var stratify = d3
	.stratify()
	.id(d => d.id)
	.parentId(d => d.id.substring(0, d.id.lastIndexOf(".")));

d3.csv("data.csv").then(data => {
	var root = stratify(data).sort(
		(a, b) => a.height - b.height || a.id.localeCompare(b.id)
	);

	console.log(data);
	console.log(root);
	console.log(tree(root));

	g.selectAll(".link")
		.data(tree(root).links())
		.enter()
		.append("path")
		.attr("class", "link")
		.attr(
			"d",
			d3
				.linkVertical()
				// .linkHorizontal()
				.x(d => d.y)
				.y(d => d.x)
		);

	var node = g
		.selectAll(".node")
		.data(root.descendants())
		.enter()
		.append("g")
		.attr("class", d => "node" + (d.children ? " node--internal" : " node--leaf"))
		.attr("transform", d => "translate(" + d.y + "," + d.x + ")");

	node.append("circle").attr("r", 2.5);

	node
		.append("text")
		.attr("dy", 3)
		.attr("x", d => (d.children ? -8 : 8))
		.style("text-anchor", d => (d.children ? "end" : "start"))
		.text(d => d.id.substring(d.id.lastIndexOf(".") + 1));
});
