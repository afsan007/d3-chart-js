new (class DonutChart {
	constructor(props) {
		this.element = props.element;
		this.key = d => d.data.region;
		this.type = d => {
			d.count = +d.count;
			return d;
		};
		this._run();
	}
	_run() {
		this.Scaffold();
		this.DataNormalize();
		this.FetchData();
	}
	Scaffold() {
		this.width = 800;
		this.height = 700;
		this.radius = Math.min(this.width, this.height) / 2;
		this.svg = d3
			.select(this.element)
			.append("svg")
			.attr("width", this.width)
			.attr("height", this.height)
			.append("g")
			.attr(
				"transform",
				"translate(" + this.width / 2 + "," + this.height / 2 + ")"
			);
	}
	DataNormalize() {
		this.color = d3.scaleOrdinal(d3.schemeSet1);
		this.pie = d3
			.pie()
			.value(d => d.count)
			.sort(null);

		this.arc = d3
			.arc()
			.innerRadius(this.radius - 80)
			.outerRadius(this.radius - 20);
	}
	Controllers() {
		this.label = d3
			.select("form")
			.selectAll("label")
			.data(this.data)
			.enter()
			.append("label");
		this.label
			.append("input")
			.attr("type", "radio")
			.attr("name", "fruit")
			.attr("value", d => d.key)
			.on("change", region => this.Update(region))
			.each(region => this.Update(region))
			.property("checked", true);
		this.label.append("span").text(d => d.key);
	}
	Update(region) {
		let vis = this;
		let path = this.svg.selectAll("path");
		let data0 = path.data();
		let data1 = this.pie(region.values);
		path = path.data(data1, this.key);
		path
			.exit()
			.datum((d, i) => this.findNeighborArc(i, data1, data0) || d)
			.transition()
			.duration(750)
			.attrTween("d", function(d) {
				let i = d3.interpolate(this._current, d);
				this._current = i(1);
				return t => vis.arc(i(t));
			})
			.remove();
		path
			.enter()
			.append("path")
			.each(function(d, i) {
				this._current = vis.findNeighborArc(i, data1, data0) || d;
			})
			.attr("fill", d => this.color(d.data.region))
			.merge(path)
			.transition()
			.duration(750)
			.attrTween("d", function(d) {
				let i = d3.interpolate(this._current, d);
				this._current = i(1);
				return t => vis.arc(i(t));
			});
	}
	findNeighborArc(i, data1, data0) {
		let d;
		return (d = this.findPreceding(i, data1, data0))
			? { startAngle: d.endAngle, endAngle: d.endAngle }
			: (d = this.findFollowing(i, data1, data0))
			? { startAngle: d.startAngle, endAngle: d.startAngle }
			: null;
	}
	findPreceding(i, data1, data0) {
		let m = data0.length;
		while (--i >= 0) {
			for (let j = 0; j < m; ++j) {
				if (this.key(data0[j]) === this.key(data1[i])) return data0[j];
			}
		}
	}
	findFollowing(i, data1, data0) {
		var newDataLength = data1.length,
			oldDataLength = data0.length;
		while (++i < newDataLength) {
			for (var j = 0; j < oldDataLength; ++j) {
				if (this.key(data0[j]) === this.key(data1[i])) return data0[j];
			}
		}
	}
	FetchData() {
		d3.tsv("data/data.tsv").then(data => {
			console.log(data);
			this.data = d3
				.nest()
				.key(d => d.fruit)
				.entries(data)
				.reverse();
			this.Controllers();
		});
	}
})({ element: "#chart" });
