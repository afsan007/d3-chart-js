// * Source:  https://blockbuilder.org/FrissAnalytics/f2796b02a5356c5f3ea59b0ffbee4db1
new (class DonutChart {
	constructor(props) {
		this.element = props.element
		this.key = d => d.data.label
		this.colorDomain = [
			'Honda',
			'Nissan',
			'Toyota',
			'Chrysler',
			'Buick',
			'Chevrolet',
			'Dodge',
			'Mercedes',
			'Volvo',
			'Ford',
			'Kia',
		]
		this.midAngle = d =>
			d.startAngle + (d.endAngle - d.startAngle) / 2
		this.run()
	}
	run() {
		this.Scaffold()
		this.DataNormalize()
		this.FetchData()
	}
	Scaffold() {
		this.width = 960
		this.height = 450
		this.radius = Math.min(this.width, this.height) / 2
		this.svg = d3
			.select(this.element)
			.append('svg')
			.attr('height', 500)
			.append('g')
		this.svg.append('g').attr('class', 'slices')
		this.svg.append('g').attr('class', 'labels')
		this.svg.append('g').attr('class', 'lines')
		this.svg.attr(
			'transform',
			'translate(' +
				this.width / 2 +
				',' +
				this.height / 2 +
				')'
		)
	}
	DataNormalize() {
		this.pie = d3
			.pie()
			.value(d => d.value)
			.sort(null)
			.padAngle(0.02)
		this.arc = d3
			.arc()
			.outerRadius(this.radius * 0.8)
			.innerRadius(this.radius * 0.4)

		this.outerArc = d3
			.arc()
			.innerRadius(this.radius * 0.9)
			.outerRadius(this.radius * 0.9)
		this.color = d3.scaleOrdinal(d3.schemeCategory10)
		// .domain(this.colorDomain)
		// .range([
		// 	"#98abc5",
		// 	"#8a89a6",
		// 	"#7b6888",
		// 	"#6b486b",
		// 	"#a05d56",
		// 	"#d0743c",
		// 	"#ff8c00"
		// ]);

		this.randomData = () =>
			// this.color
			// 	.domain()
			this.colorDomain.map(label => ({
				label,
				value: Math.random(),
			}))
	}
	TextLabels(data) {
		var text = this.svg
			.select('.labels')
			.selectAll('text')
			.data(this.pie(data), this.key)

		text
			.enter()
			.append('text')
			.attr('dy', '.35em')
			.text(d => d.data.label)
			// with our merge =>> Only the entering elements have their text updated
			// 					  because we call .text on the enter selection.
			.merge(text)
			.transition()
			.duration(1000)
			.attrTween('transform', d => {
				this._current = this._current || d
				var interpolate = d3.interpolate(this._current, d)
				this._current = interpolate(0)
				return t => {
					var d2 = interpolate(t)
					var pos = this.outerArc.centroid(d2)
					pos[0] =
						this.radius * (this.midAngle(d2) < Math.PI ? 1 : -1)
					return 'translate(' + pos + ')'
				}
			})
			.styleTween('text-anchor', d => {
				this._current = this._current || d
				var interpolate = d3.interpolate(this._current, d)
				this._current = interpolate(0)
				return t => {
					var d2 = interpolate(t)
					return this.midAngle(d2) < Math.PI ? 'start' : 'end'
				}
			})

		text.exit().remove()
	}
	PieSlices(data) {
		let vis = this
		var slice = this.svg
			.select('.slices')
			.selectAll('path.slice')
			.data(this.pie(data), this.key)
		slice
			.enter()
			.insert('path')
			.style('fill', d => this.color(d.data.label))
			.attr('class', 'slice')
			.merge(slice)
			.transition()
			.duration(1000)
			.attrTween('d', function (d) {
				this._current = this._current || d
				var interpolate = d3.interpolate(this._current, d)
				this._current = interpolate(0)
				return t => vis.arc(interpolate(t))
			})
		slice.exit().remove()
	}
	Lines(data) {
		var polyline = this.svg
			.select('.lines')
			.selectAll('polyline')
			.data(this.pie(data), this.key)
		polyline.exit().remove()
		polyline
			.enter()
			.append('polyline')
			.merge(polyline)
			.transition()
			.duration(1000)
			.attrTween('points', d => {
				this._current = this._current || d
				var interpolate = d3.interpolate(this._current, d)
				this._current = interpolate(0)
				console.log(this._current)
				return t => {
					var d2 = interpolate(t)
					var pos = this.outerArc.centroid(d2)
					pos[0] =
						this.radius *
						// 0.95 *
						1 *
						(this.midAngle(d2) < Math.PI ? 1 : -1)
					return [
						this.arc.centroid(d2),
						this.outerArc.centroid(d2),
						pos,
					]
				}
			})
	}
	FetchData() {
		this.Update(this.randomData())
		d3
			.select('.randomize')
			.on('click', () => this.Update(this.randomData()))
	}
	Update(data) {
		/* ------- PIE SLICES -------*/
		this.PieSlices(data)
		/* ------- TEXT LABELS -------*/
		this.TextLabels(data)
		/* ------- SLICE TO TEXT POLYLINE -------*/
		this.Lines(data)
	}
})({ element: '.chart' })

/* Update Pattern
	[
		enter	=>	Enter selected Data to dom(.enter)
		update	=>	Update data (selection self)
		exit	=>	Exit removed data from dom(.exit)
	]
*/
