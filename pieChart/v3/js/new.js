new (class StackAreaChart {
	constructor(props) {
		this.element = props.element
		this.ctrl = props.controller
		this.current = {
			Run: 12,
			Experiment: 6,
		}
		this.groupBy = ['Run', 'Experiment', 'Speed']
		this._run()
	}
	_run() {
		this.FetchData()
	}
	Scaffold() {
		this.width = 900
		this.height = 800
		this.radius =
			Math.min(this.height, this.width) * 0.5 - 100
		this.labelOffset = 10
		this.svg = d3
			.select(this.element)
			.append('svg')
			.attr('width', this.width)
			.attr('height', this.height)
			.append('g')
			.attr(
				'transform',
				`translate(${this.width / 2}, ${this.height / 2})`
			)
		this.arc = d3
			.arc()
			.outerRadius(this.radius)
			.innerRadius(0)
		this.colourSchema = [
			'#fff',
			'#e6194b',
			'#3cb44b',
			'#ffe119',
			'#4363d8',
			'#f58231',
			'#911eb4',
			'#46f0f0',
			'#f032e6',
			'#bcf60c',
			'#fabebe',
			'#008080',
			'#e6beff',
			'#9a6324',
			'#fffac8',
			'#800000',
			'#aaffc3',
			'#808000',
			'#ffd8b1',
			'#000075',
			'#808080',
		]
	}
	AddControls(group, data) {
		let vis = this
		const container = d3
			.select(this.ctrl)
			.append('fieldset')
			.classed('ctrl--' + group, true)
		container
			.append('h4')
			.text(
				'Number of ' + group.toLowerCase() + 's to display'
			)
		container
			.append('label')
			.text(d3.min(data, d => d[group]))
			.classed('label--inline label--min', true)
		container
			.append('input')
			.attr('id', 'input--' + group)
			.attr('type', 'range')
			.attr('name', group)
			.attr(
				'min',
				d3.min(data, d => d[group])
			)
			.attr(
				'max',
				d3.max(data, d => d[group])
			)
			.attr('value', this.current[group])
			.attr('step', 1)
			.attr('list', 'tickMarks--' + group)
			.on('change', function () {
				vis.current[this.name] = this.value
				vis.Draw(
					data.filter(
						d =>
							d.Run <= vis.current.Run &&
							d.Experiment <= vis.current.Experiment
					)
				)
			})
		container
			.append('label')
			.text(d3.max(data, d => d[group]))
			.classed('label--inline label--max', true)
	}
	GrouperSwitch() {
		let container = d3
			.select(this.ctrl)
			.append('fieldset')
			.classed('ctrl--switch', true)

		container.append('h4').text('Switch grouping')

		container
			.append('p')
			.attr('id', 'grouping_text')
			.text(
				`Group by ${this.groupBy[0]} then ${this.groupBy[1]}`
			)

		container
			.append('button')
			.text('Switch!')
			.on('click', () => {
				this.groupBy.splice(1, 0, this.groupBy.shift())
				d3
					.select('#grouping_text')
					.text(
						`Group by ${this.groupBy[0]} then ${this.groupBy[1]}`
					)
				this.Draw(
					this.data.filter(
						d =>
							d.Run <= this.current.Run &&
							d.Experiment <= this.current.Experiment
					)
				)
			})
	}
	FetchData() {
		d3
			.csv('data/data.csv', d => ({
				Run: +d.Run,
				Experiment: +d.Expt,
				Speed: +d.Speed,
			}))
			.then(data => {
				this.data = data
				this.Run(this.data)
			})
	}
	DataNormalize(dataset) {
		this.segScale = d3
			.scaleLinear()
			.domain(
				d3.extent(dataset, d => d[this.groupBy.slice(-1)[0]])
			)

		this.nested = d3
			.nest()
			.key(d => d[this.groupBy[0]])
			.entries(dataset)

		this.pie = d3
			.pie()
			.value(d =>
				d3.sum(d.values, e => e[this.groupBy.slice(-1)[0]])
			)
			.sort((a, b) => a.key - b.key)(this.nested)
		this.pie.forEach(d => {
			console.log(this.groupBy.slice(-1)[0])
			d.children = d3
				.pie()
				.value(e => e[this.groupBy.slice(-1)[0]])
				.sort((a, b) => a[this.groupBy[1] - b[this.groupBy[1]]])
				.startAngle(d.startAngle)
				.endAngle(d.endAngle)(d.data.values)
		})
		this.sliceArc = d3.arc().innerRadius(0)
	}
	Slice() {
		const slices = this.svg
			.selectAll('.slice')
			.data(this.pie, d => d.key)
		this.sliceEnter = slices
			.enter()
			.append('g')
			.classed('slice', true)

		this.sliceEnter
			.append('text')
			.classed('label', true)
			.merge(slices)
			.transition()
			.duration(1000)
			.attr('text-anchor', d => {
				d.midPt = (d.startAngle + d.endAngle) / 2
				return d.midPt < Math.PI ? 'start' : 'end'
			})
			.attr(
				'x',
				d =>
					d3.pointRadial(
						d.midPt,
						this.radius + this.labelOffset
					)[0]
			)
			.attr(
				'y',
				d =>
					d3.pointRadial(
						d.midPt,
						this.radius + this.labelOffset
					)[1]
			)
			.attr('dy', d => {
				let dy = 0.35
				if (d.midPt < 0.5 * Math.PI || d.midPt > 1.5 * Math.PI)
					dy -= 3.0
				return dy
			})
			.text(d => {
				console.log(d)
				const ext = d3.extent(
					d.data.values.map(e => e[this.groupBy[1]])
				)
				return (
					`${this.groupBy[0]}${d.data.key},${this.groupBy[1]}` +
					(ext[0] === ext[1]
						? `${ext[0]}`
						: 's ' + ext.join(' - '))
				)
			})
	}
	Segment() {
		let segment = this.sliceEnter
			.selectAll('.segment')
			.data(d => d.children)
		let segmentEnter = segment
			.enter()
			.append('g')
			.classed('segment', true)

		segmentEnter
			.append('path')
			.classed('segment--path', true)
			.attr('d', d =>
				this.sliceArc.outerRadius(
					this.segScale(d.data[this.groupBy.slice(-1)[0]]) *
						this.radius *
						0.5 +
						0.5 * this.radius
				)(d)
			)
			.attr('fill', (d, i) => {
				const color = d3.rgb(
					this.colourSchema[d.data[this.groupBy[0]]]
				)
				return color.brighter(i / this.current[this.groupBy[0]])
			})
			.append('title')
			.text(d =>
				this.groupBy.map(e => e + ': ' + d.data[e]).join(', ')
			)

		// segmentEnter
		// 	.append('line')
		// 	.attr('y2', this.radius)
		// 	.attr(
		// 		'class',
		// 		(d, i) =>
		// 			'segment--line slice--' +
		// 			d.data[this.groupBy[0]] +
		// 			' seg-' +
		// 			d.data[this.groupBy[1]] +
		// 			'seg-' +
		// 			(i === 0 ? 'border' : 'inner')
		// 	)
		// 	.attr(
		// 		'transform',
		// 		d =>
		// 			'rotate(' +
		// 			(180 + (d.startAngle * 180) / Math.PI) +
		// 			')'
		// 	)
	}
	Draw(dataset) {
		this.svg.selectAll('.slice').remove()
		this.DataNormalize(dataset)
		this.Slice()
		this.Segment()
	}
	Run() {
		this.AddControls(this.groupBy[0], this.data)
		this.AddControls(this.groupBy[1], this.data)
		this.GrouperSwitch()
		this.Scaffold()
		this.Draw(
			this.data.filter(
				d =>
					d.Run <= this.current.Run &&
					d.Experiment <= this.current.Experiment
			)
		)
	}
})({ element: '#chart', controller: '#ctrl' })
