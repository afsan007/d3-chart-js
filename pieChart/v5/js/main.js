new (class Dashboard {
	constructor(props) {
		this.element = props.element
		this.segColor = c =>
			({
				low: '#807dba',
				mid: '#e08214',
				high: '#41ab5d',
			}[c])
		this._run()
	}
	_run() {
		this.Scaffold()
		this.FetchData()
	}
	Scaffold() {
		this.hG = {}
		this.hGDim = { t: 60, r: 0, b: 30, l: 0 }
		this.hGDim.w = 500 - this.hGDim.l - this.hGDim.r
		this.hGDim.h = 300 - this.hGDim.t - this.hGDim.b

		this.hGsvg = d3
			.select(this.element)
			.append('svg')
			.attr(
				'width',
				this.hGDim.w + this.hGDim.l + this.hGDim.r
			)
			.attr(
				'height',
				this.hGDim.h + this.hGDim.t + this.hGDim.b
			)
			.attr('class', 'histogram')
			.append('g')
			.attr(
				'transform',
				'translate(' + this.hGDim.l + ',' + this.hGDim.t + ')'
			)

		this.pC = {}
		this.pieDim = {
			w: 250,
			h: 250,
		}
		this.pieDim.r = Math.min(this.pieDim.w, this.pieDim.h) / 2
		this.pieSvg = d3
			.select(this.element)
			.append('svg')
			.attr('width', this.pieDim.w)
			.attr('height', this.pieDim.h)
			.attr('class', 'pieChart')
			.append('g')
			.attr(
				'transform',
				'translate(' +
					this.pieDim.w / 2 +
					',' +
					this.pieDim.h / 2 +
					')'
			)
	}
	FetchData() {
		d3.json('data/data.json').then(data => {
			this.data = data
			data.forEach(
				d => (d.total = d.freq.low + d.freq.mid + d.freq.high)
			)
			this.totalFreq = ['low', 'mid', 'high'].map(d => ({
				type: d,
				freq: d3.sum(data.map(t => t.freq[d])),
			}))
			this.stateFreq = data.map(d => [d.State, d.total])
			this.PieChart(this.totalFreq)
			this.HistoGram(this.stateFreq)
			this.Legend(this.totalFreq)
		})
	}
	HG_DataNormalize(dataset) {
		let x = d3
			.scaleBand()
			.rangeRound([0, this.hGDim.w])
			.padding(0.1)
			.domain(dataset.map(d => d[0]))
		let y = d3
			.scaleLinear()
			.range([this.hGDim.h, 0])
			.domain([0, d3.max(dataset, d => d[1])])
		return { x, y }
	}
	HG_Axises(x) {
		this.hGsvg
			.append('g')
			.attr('class', 'x axis')
			.attr('transform', 'translate(0,' + this.hGDim.h + ')')
			.call(d3.axisBottom().scale(x))
	}
	HG_Events() {
		let mouseover = d => {
			let state = this.data.filter(el => el.State === d[0])[0]
			let newData = d3
				.keys(state.freq)
				.map(s => ({ type: s, freq: state.freq[s] }))
			console.log(newData)
			this.leg.update(newData)
			this.pC.update(newData)
		}
		let mouseout = d => {
			this.leg.update(this.totalFreq)
			this.pC.update(this.totalFreq)
		}
		return { mouseout, mouseover }
	}
	HistoGram(dataset) {
		console.log(dataset)
		const { x, y } = this.HG_DataNormalize(dataset)
		const { mouseout, mouseover } = this.HG_Events()
		this.HG_Axises(x)

		let bar = this.hGsvg.selectAll('.bar').data(dataset)
		let barGroup = bar
			.enter()
			.append('g')
			.attr('class', 'bar')

		barGroup
			.append('rect')
			.attr('width', x.bandwidth)
			.attr('height', d => this.hGDim.h - y(d[1]))
			.attr('x', d => x(d[0]))
			.attr('y', d => y(d[1]))
			.attr('fill', 'steelblue')
			.on('mouseover', mouseover)
			.on('mouseout', mouseout)

		barGroup
			.append('text')
			.text(d => d3.format(',')(d[1]))
			.attr('text-anchor', 'middle')
			.attr('x', d => x(d[0]) + x.bandwidth() / 2)
			.attr('y', d => y(d[1]) - 5)

		this.hG.update = (newData, color) => {
			console.log(newData)
			y.domain([0, d3.max(newData, d => d[1])])
			let bars = this.hGsvg.selectAll('.bar').data(newData)

			bars
				.select('rect')
				.transition()
				.duration(500)
				.attr('y', d => y(d[1]))
				.attr('height', d => this.hGDim.h - y(d[1]))
				.attr('fill', color)
			bars
				.select('text')
				.transition()
				.duration(500)
				.text(d => d3.format(',')(d[1]))
				.attr('x', d => x(d[0]) + x.bandwidth() / 2)
				.attr('y', d => y(d[1]) - 5)
		}
	}
	PC_DataNormalize() {
		// create function to draw the arcs of the pie slices.
		let arc = d3
			.arc()
			.outerRadius(this.pieDim.r - 10)
			.innerRadius(0)

		// create a function to compute the pie slice angles.
		let pie = d3
			.pie()
			.sort(null)
			.value(d => d.freq)
		return { pie, arc }
	}
	PC_Events() {
		let mouseover = d =>
			this.hG.update(
				this.data.map(el => [el.State, el.freq[d.data.type]]),
				this.segColor(d.data.type)
			)

		let mouseout = d =>
			this.hG.update(
				this.data.map(el => [el.State, el.total]),
				'steelblue'
			)
		return { mouseover, mouseout }
	}
	PieChart(dataset) {
		const { pie, arc } = this.PC_DataNormalize()
		const { mouseover, mouseout } = this.PC_Events()
		function arcTween(d) {
			let i = d3.interpolate(this._current, d)
			this._current = i(1)
			return t => arc(i(t))
		}
		this.pieSvg
			.selectAll('path')
			.data(pie(dataset))
			.enter()
			.append('path')
			.each(function (d) {
				this._current = d
			})
			.style('fill', d => this.segColor(d.data.type))
			.on('mouseover', mouseover)
			.on('mouseout', mouseout)
			.transition()
			.duration(500)
			.attrTween('d', arcTween)

		this.pC.update = newData => {
			this.pieSvg
				.selectAll('path')
				.data(pie(newData))
				.transition()
				.duration(500)
				.attrTween('d', arcTween)
		}
	}
	Legend(dataset) {
		// Utility function to compute percentage.
		const getLegend = (d, specData) =>
			d3.format('%')(
				d.freq / d3.sum(specData.map(v => v.freq))
			)

		this.leg = {}
		let legend = d3
			.select(this.element)
			.append('table')
			.attr('class', 'legend')
		let tr = legend
			.append('tbody')
			.selectAll('tr')
			.data(dataset)
			.enter()
			.append('tr')
		let td = tr
			.append('td')
			.append('svg')
			.attr('width', '16')
			.attr('height', '16')
			.append('rect')
			.attr('width', '16')
			.attr('height', '16')
			.attr('fill', d => this.segColor(d.type))

		tr.append('td').text(d => d.type)

		tr
			.append('td')
			.attr('class', 'legendFreq')
			.text(d => d3.format(',')(d.freq))
		tr
			.append('td')
			.attr('class', 'legendPerc')
			.text(d => getLegend(d, dataset))
		this.leg.update = newData => {
			var l = legend
				.select('tbody')
				.selectAll('tr')
				.data(newData)
			console.log(l)
			l.select('.legendFreq').text(d => d3.format(',')(d.freq))
			l.select('.legendPerc').text(d => getLegend(d, newData))
		}
	}
})({ element: '#dashboard' })
