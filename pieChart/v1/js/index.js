new (class PieChart {
	constructor(props) {
		this.run();
	}

	run() {
		this.Scaffold();
		this.DataNormalize();
		this.FetchData();
	}

	Scaffold() {
		this.width = 800;
		this.height = 600;
		this.svg = d3
			.select('.chart')
			.append('svg')
			.attr('width', this.width)
			.attr('height', this.height)
			.append('g')
			.attr(
				'transform',
				'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
			);
		this.radius = Math.min(this.width, this.height) / 2;
	}

	DataNormalize() {
		this.color = d3.scaleOrdinal([
			'#98abc5',
			'#8a89a6',
			'#7b6888',
			'#6b486b',
			'#a05d56',
			'#d0743c',
			'#ff8c00'
		]);

		this.pie = d3.pie().value(d => d.population);
		this.path = d3
			.arc()
			.outerRadius(this.radius - 10)
			.innerRadius(0);

		this.label = d3
			.arc()
			.outerRadius(this.radius - 100)
			.innerRadius(this.radius - 40);
	}

	FetchData() {
		d3.csv('data/data.csv', d => {
			d.population = +d.population;
			return d;
		}).then(data => {
			console.log(data);
			this.Update(data);
		});
	}

	Update(data) {
		var arc = this.svg
			.selectAll('.arc')
			.data(this.pie(data))
			.enter()
			.append('g')
			.attr('class', 'arc');

		arc
			.append('path')
			.attr('d', this.path)
			.attr('fill', d => this.color(d.data.age));

		arc
			.append('text')
			.attr('transform', d => 'translate(' + this.label.centroid(d) + ')')
			.attr('dy', '0.35em')
			.text(d => d.data.age);
	}
})();
