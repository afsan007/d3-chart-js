console.clear();
new (class chart {
	constructor(props) {
		this.el = props;
		this.width = 720;
		this.height = 750;
		this._run();
	}
	_run() {
		this.Scaffold();
		this.DataProvision();
		this.Operation();
	}
	Scaffold() {
		this.m = { l: 0, t: 10, r: 10, b: 10 };
		this.svg = d3
			.select(this.el)
			.append("svg")
			.attr("width", this.width - this.m.l - this.m.r)
			.attr("height", this.height - this.m.t - this.m.b);
		this.g = this.svg
			.append("g")
			.attr("transform", "translate(" + this.m.l + "," + this.m.t + ")");
	}
	DataProvision() {
		this.radius = 6;
		let step = this.radius * 2;
		let theta = Math.PI * (3 - Math.sqrt(5));

		this.data = d3.range(800).map((_, i) => {
			let r = step * Math.sqrt((i += 0.5));
			let a = theta * i;
			let cx = this.width / 2 + r * Math.cos(a);
			let cy = this.height / 2 + r * Math.sin(a);
			return [cx, cy];
		});
		// let data = Array.from({ length: 800 }, (_, i) => {
		// 	const r = step * Math.sqrt((i += 0.5)),
		// 		a = theta * i;
		// 	return [this.width / 2 + r * Math.cos(a), this.height / 2 + r * Math.sin(a)];
		// });
		this.bubbles = this.g.selectAll("circle").data(this.data);
		this.bubbles
			.enter()
			.append("circle")
			.attr("r", this.radius)
			.attr("cx", ([x, y]) => x)
			.attr("cy", ([x, y]) => y)
			.attr("fill", (d, i) => d3.interpolateSinebow(i / 360));
	}
	Operation() {
		let view = [this.width / 2, this.height / 2, this.height];

		let transform = ([x, y, r]) =>
			`translate(${this.width / 2},${this.height / 2}),scale(${
				this.height / r
			}),translate(${-x},${-y})`;

		let currentTransform = [this.width / 2, this.height / 2, this.height];
		let transition = () => {
			let randomBubble = this.data[Math.floor(Math.random() * this.data.length)];
			let i = d3.interpolateZoom(currentTransform, [
				...randomBubble,
				this.radius * 2 + 1,
			]);
			this.g
				.transition()
				.duration(i.duration)
				.attrTween("transform", () => t => transform((currentTransform = i(t))));
		};
		d3.select("#butt").on("click", transition);
	}
})("#chart");
