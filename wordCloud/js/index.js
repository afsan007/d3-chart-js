var shuffling = true; // shuffling make less-sparsed word clouds
var spiral = "archimedean"; // 'archimedean or 'rectangular'
var outter = true; // (try to) make disappearing words outter

var city = "amsterdam";
var zoom_group; // reusable d3 selection

var words = {};

Promise.all([d3.json("data/amsterdam.json"), d3.json("data/london.json")]).then(
	([words_amsterdam, words_london]) => {
		document.querySelector("#select_city").addEventListener("change", () => {
			update();
		});

		Object.keys(words_amsterdam).forEach(function(word_lemma) {
			words[word_lemma] = {
				amsterdam: words_amsterdam[word_lemma],
				london: { polarity: words_amsterdam[word_lemma].polarity, count: 0 }
			};
		});

		Object.keys(words_london).forEach(function(word_lemma) {
			if (words[word_lemma]) {
				words[word_lemma].london = words_london[word_lemma];
			} else {
				w = words[word_lemma] = {
					amsterdam: { polarity: words_london[word_lemma].polarity, count: 0 },
					london: words_london[word_lemma]
				};
			}
		});
		console.log("words", words);
		Math.seedrandom("abcde");
		// define a fixed random seed, to avoid to have a different layout on each page reload.
		// change the string to randomize

		var words_frequency = [];
		var max = -Infinity;
		Object.keys(words).forEach(function(word_lemma) {
			let o = words[word_lemma];
			o.lemma = word_lemma;
			o.maxCount = Math.max(o.amsterdam.count, o.london.count);
			words_frequency.push(o);
			if (max < o.maxCount) {
				max = o.maxCount;
			}
		});
		console.log("max: " + max);

		var font_size = d3
			.scaleLinear()
			.domain([1, max])
			.range([10, 100]);

		var color = d3
			.scaleLinear()
			.domain([-max, 0, max])
			.range([d3.hcl(36, 65, 50), d3.hcl(95, 65, 80), d3.hcl(150, 65, 50)])
			.interpolate(d3.interpolateHcl);

		// var color = d3
		// 	.scaleQuantize()
		// 	.domain([-max, max])
		// 	.range([d3.hcl(36, 65, 50), d3.hcl(150, 65, 50)])

		if (shuffling) {
			shuffle(words_frequency);
		}
		words_frequency.sort(function(a, b) {
			if (outter) {
				return (
					b.maxCount -
					a.maxCount +
					(b.amsterdam.count === 0 || b.london.count === 0 ? 1 : 0)
				);
			} else {
				return b.maxCount - a.maxCount;
			}
		});
		console.log(words_frequency);
		d3.layout
			.cloud()
			.size([960, 500])
			.words(words_frequency)
			.rotate(function() {
				return ~~(Math.random() * 2) * 90;
			})
			.font("Impact")
			.spiral(spiral)
			.text(function(d) {
				return d.lemma;
			})
			.fontSize(function(d) {
				return font_size(d.maxCount);
			})
			.on("end", draw)
			.start();

		function draw(words) {
			var svg = d3
				.select("body")
				.append("svg")
				.attr("width", 960)
				.attr("height", 500);

			// append a group for zoomable content
			zoom_group = svg.append("g");

			// define a zoom behavior
			var zoom = d3
				.zoom()
				.scaleExtent([1, 4]) // min-max zoom
				.on("zoom", () => zoom_group.attr("transform", d3.event.transform));

			// bind the zoom behavior to the main SVG
			svg.call(zoom);

			zoom_group
				.append("g")
				.attr("transform", "translate(480,250)")
				.selectAll("text")
				.data(words)
				.enter()
				.append("text")
				.style("font-size", function(d) {
					return font_size(d[city].count) + "px";
				})
				.style("font-family", "Impact")
				.style("fill", function(d) {
					return color(d[city].polarity);
				})
				.style("fill-opacity", function(d) {
					return d[city].count > 0 ? 1 : 0;
				})
				.attr("text-anchor", "middle")
				.attr("transform", function(d) {
					var far = 1500 * (Math.random() > 0.5 ? +1 : -1);
					if (d.rotate === 0)
						return "translate(" + far + ",0)rotate(" + d.rotate + ")";
					else return "translate(0," + far + ")rotate(" + d.rotate + ")";
				})
				.text(function(d) {
					return d.lemma;
				})
				.transition()
				.duration(2000)
				.attr("transform", function(d) {
					return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
				});
		}

		function update() {
			city = city === "london" ? "amsterdam" : "london";

			zoom_group
				.selectAll("text")
				.data(words_frequency)
				.transition()
				.duration(1000)
				.style("font-size", function(d) {
					return font_size(d[city].count) + "px";
				})
				.style("fill", function(d) {
					return color(d[city].polarity);
				})
				.style("fill-opacity", function(d) {
					return d[city].count > 0 ? 1 : 0;
				});
		}

		function shuffle(array) {
			let counter = array.length;

			// While there are elements in the array
			while (counter > 0) {
				// Pick a random index
				let index = Math.floor(Math.random() * counter);

				// Decrease counter by 1
				counter--;

				// And swap the last element with it
				let temp = array[counter];
				array[counter] = array[index];
				array[index] = temp;
			}

			return array;
		}
	}
);
