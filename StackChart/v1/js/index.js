'use-strict';

class StackAreaChart {
	constructor(props) {
		this.run();
	}

	run() {
		this.Scaffold();
		this.Axises();
		this.DataNormalize();
		this.FetchData();
	}

	Scaffold() {
		this.margin = { top: 120, right: 150, bottom: 30, left: 50 };
		this.width = 890 - this.margin.left - this.margin.right;
		this.height = 700 - this.margin.top - this.margin.bottom;
		this.svg = d3
			.select('.chart')
			.append('svg')
			.attr('width', this.width + this.margin.left + this.margin.right)
			.attr('height', this.height + this.margin.top + this.margin.bottom)
			.append('g')
			.attr(
				'transform',
				'translate(' + this.margin.left + ',' + this.margin.top + ')'
			);
		this.parseData = d3.timeParse('%Y %b %d');
	}

	Axises() {
		this.xAxis = this.svg
			.append('g')
			.attr('class', 'x axis')
			.attr('transform', 'translate(0,' + this.height + ')');
		this.yAxis = this.svg.append('g').attr('class', 'y axis');
		this.xAxisCall = d3.axisBottom();
		this.yAxisCall = d3.axisLeft().ticks(10, '%');
	}

	Labels() {
		this.Legend = this.svg
			.append('g')
			.attr('class', 'legend')
			.attr(
				'transform',
				'translate(' + (this.width + 130) + ',' + this.height / 4 + ')'
			);
		this.data.columns.shift();
		this.data.columns.forEach((item, index) => {
			let container = this.Legend.append('g').attr(
				'transform',
				'translate(0,' + index * 30 + ')'
			);

			container
				// .append('rect')
				// .attr('width', 20)
				// .attr('height', 20)
				.append('circle')
				.attr('cx', 10)
				.attr('cy', 10)
				.attr('r', 10)
				.attr('fill', this.z(item));

			container
				.append('text')
				.attr('x', -10)
				.attr('y', 15)
				.attr('class', 'legend text')
				.attr('text-anchor', 'end')
				.attr('fill', '#fff')
				.style('text-transform', 'capitalize')
				.text(item);
			console.log(container);
		});
	}

	DataNormalize() {
		this.y = d3.scaleLinear().range([this.height, 0]);
		this.x = d3.scaleTime().range([0, this.width]);
		this.z = d3.scaleOrdinal().range(d3.schemePaired);
		this.area = d3
			.area()
			.x(d => this.x(d.data.date))
			.y0(d => this.y(d[0]))
			.y1(d => this.y(d[1]));
		this.stack = d3.stack();
		// .order(d3.stackOrderAppearance)
		// .offset(d3.stackOffsetNone);
	}

	FetchData() {
		const type = (d, i, columns) => {
			d.date = this.parseData(d.date);
			for (let i = 0, n = columns.length; i < n; i++)
				d[columns[i]] = d[columns[i]] / 100;
			return d;
		};
		d3.tsv('data/stacked_area1.tsv', type)
			.then(data => {
				this.data = data;
				this.Update(this.data);
			})
			.catch(console.log);
	}

	Update() {
		console.log(this.data);
		let keys = this.data.columns.slice(1);

		this.xAxis.call(this.xAxisCall.scale(this.x));
		this.yAxis.call(this.yAxisCall.scale(this.y));

		this.x.domain(d3.extent(this.data, d => d.date));
		this.z.domain(keys);
		this.stack.keys(keys);

		this.layer = this.svg
			.selectAll()
			.data(this.stack(this.data))
			.enter()
			.append('g')
			.attr('class', 'layer');

		this.layer
			.append('path')
			.attr('class', 'area')
			.style('fill', d => this.z(d.key))
			.attr('d', this.area);
		this.Labels();
	}
}

new StackAreaChart();
