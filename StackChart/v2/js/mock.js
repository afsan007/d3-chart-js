var margin = { top: 10, right: 20, bottom: 20, left: 60 },
	width = 960 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

var svg = d3
	.select('body')
	.append('svg')
	.attr('width', width + margin.left + margin.right)
	.attr('height', height + margin.top + margin.bottom)
	.append('g')
	.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

var g = svg.append('g');

var allVehicleTypeKeys = [
	'generator',
	'hybrid car',
	'car',
	'hybrid SUV',
	'van, pickup, SUV',
	'truck (<18 wheels)',
	'18 wheeler',
	'bus'
];
var allFuelTypeKeys = ['diesel', 'B20', 'gasoline', 'B100', 'CNG'];

d3.csv(
	'ImpactOfFuel.csv',
	function(d, i, columns) {
		for (i = 1, t = 0; i < columns.length; ++i)
			t += d[columns[i]] = +d[columns[i]];
		d.total = t;
		return d;
	},
	function(error, data) {
		if (error) throw error;
		console.log('original', data);
		data.sort(function(a, b) {
			return b.total - a.total;
		});

		var yStackMax = d3.max(data, function(d) {
			return d.total;
		});
		var z = d3
			.scaleOrdinal()
			.domain(allVehicleTypeKeys)
			.range([
				'#98abc5',
				'#8a89a6',
				'#7b6888',
				'#6b486b',
				'#a05d56',
				'#d0743c',
				'#ff8c00',
				'#ff8c00'
			]);
		var y = d3
			.scaleLinear()
			.domain([0, yStackMax])
			.rangeRound([height, 0]);
		var x = d3
			.scaleBand()
			.domain(
				data.map(function(d) {
					return d.vehicleType;
				})
			)
			.rangeRound([0, width])
			.paddingInner(0.05)
			.align(0.1);

		svg
			.append('g')
			.attr('class', 'xAxis')
			.attr('transform', 'translate(0,' + height + ')')
			.call(d3.axisBottom(x));

		g.append('g')
			.selectAll('g')
			.data(d3.stack().keys(allFuelTypeKeys)(data))
			.enter()
			.append('g')
			.attr('fill', function(d) {
				return z(d.key);
			})
			.selectAll('rect')
			.data(d => d)
			.enter()
			.append('rect')
			.attr('x', d => x(d.data.vehicleType))
			.attr('y', d => y(d[1]))
			.attr('height', d => y(d[0]) - y(d[1]))
			.attr('width', x.bandwidth())
			.on('mouseover', () => tooltip.style('display', null))
			.on('mouseout', function() {
				tooltip.style('display', 'none');
			})
			.on('mousemove', function(d) {
				console.log(d);
				var xPosition = d3.mouse(this)[0] - 5;
				var yPosition = d3.mouse(this)[1] - 5;
				tooltip.attr(
					'transform',
					'translate(' + xPosition + ',' + yPosition + ')'
				);
				tooltip.select('text').text(d[1] - d[0]);
			});

		var legend = g
			.append('g')
			.attr('font-family', 'sans-serif')
			.attr('font-size', 10)
			.attr('text-anchor', 'end')
			.selectAll('g')
			.data(allFuelTypeKeys.slice().reverse())
			.enter()
			.append('g')
			.attr('transform', function(d, i) {
				return 'translate(0,' + i * 20 + ')';
			});

		legend
			.append('rect')
			.attr('x', width - 19)
			.attr('width', 19)
			.attr('height', 19)
			.attr('fill', z);

		legend
			.append('text')
			.attr('x', width - 24)
			.attr('y', 9.5)
			.attr('dy', '0.32em')
			.text(function(d) {
				return d;
			});

		// Prep the tooltip bits, initial display is hidden
		var tooltip = svg
			.append('g')
			.attr('class', 'tooltip')
			.style('display', 'none');

		tooltip
			.append('rect')
			.attr('width', 60)
			.attr('height', 20)
			.attr('fill', 'white')
			.style('opacity', 0.5);

		tooltip
			.append('text')
			.attr('x', 30)
			.attr('dy', '1.2em')
			.style('text-anchor', 'middle')
			.attr('font-size', '12px')
			.attr('font-weight', 'bold');

		// Playing with nesting
		var nestByVehicleType = d3
			.nest()
			.key(function(d) {
				return d.vehicleType;
			})
			.entries(data);
		console.log('nest by vehicle type', nestByVehicleType);
		var numberOfLayers = allFuelTypeKeys.length;
		var stack = d3.stack().keys(allFuelTypeKeys);
		console.log('stack data', stack(data));
		console.log('stack data nest by Vehicle Type', stack(nestByVehicleType));
	}
);
