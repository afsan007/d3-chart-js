let toggle = false;
class StackAreaChart {
	constructor() {
		this.run();
		this.FetchData();
		this.colorRange = [
			'#98abc5',
			'#8a89a6',
			'#7b6888',
			'#6b486b',
			'#a05d56',
			'#d0743c',
			'#ff8c00',
			'#ff8c00'
		];
	}

	run() {
		this.Scaffold();
		this.Axises();
		this.DataNormalizer();
	}

	Scaffold() {
		this.margin = { top: 120, right: 50, bottom: 30, left: 50 };
		this.width = 890 - this.margin.left - this.margin.right;
		this.height = 700 - this.margin.top - this.margin.bottom;
		this.svg = d3
			.select('.chart')
			.append('svg')
			.attr('width', this.width + this.margin.left + this.margin.right)
			.attr('height', this.height + this.margin.top + this.margin.bottom)
			.append('g')
			.attr(
				'transform',
				'translate(' + this.margin.left + ',' + this.margin.top + ')'
			);
	}

	Axises() {
		this.xAxisCall = d3.axisBottom();
		this.xAxis = this.svg
			.append('g')
			.attr('class', 'xAxis')
			.attr('transform', 'translate(0,' + this.height + ')');
	}

	DataNormalizer() {
		this.x = d3
			.scaleBand()
			.rangeRound([0, this.width])
			.paddingInner(0.05)
			.align(0.1);
		this.y = d3.scaleLinear().range([this.height, 0]);
		this.vehicleColor = d3.scaleOrdinal();
		this.stack = d3.stack();
	}

	Legends() {
		this.Legend = this.svg
			.append('g')
			.attr('transform', 'translate(' + (this.width - 20) + ',0)');
		this.data.columns.forEach((elem, index) => {
			let container = this.Legend.append('g').attr(
				'transform',
				'translate(0,' + index * 20 + ')'
			);
			container
				.append('circle')
				.attr('cx', 15)
				.attr('cy', -4)
				.attr('r', 7)
				.attr('fill', this.vehicleColor(elem));
			container
				.append('text')
				.attr('class', 'text')
				.attr('text-anchor', 'end')
				.text(elem);
		});
	}

	FetchData() {
		Promise.all([
			d3.csv('data/data.csv', type),
			d3.csv('data/newData.csv', type)
		])
			.then(([data, newData]) => {
				data.sort((a, b) => b.total - a.total);
				newData.sort((a, b) => b.total - a.total);
				console.log(data);
				// setInterval(() => {
				toggle = !toggle;
				this.data = toggle ? data : newData;
				this.Update(this.data);
				// }, 2000);
				this.Legends();
			})
			.catch(console.log);
	}
	toolTip() {}
	Update() {
		this.data.columns.shift();
		// Scale
		this.x.domain(this.data.map(d => d.vehicleType));
		this.y.domain([0, d3.max(this.data, d => d.total)]);
		this.vehicleColor
			.range(this.colorRange)
			.domain(this.data.map(d => d.vehicleType));
		this.stack.keys(this.data.columns)(this.data);

		// Axises
		this.xAxis.call(this.xAxisCall.scale(this.x));

		// JOIN new data with old elements.
		this.g = this.svg.append('g');

		let rects = this.g
			.selectAll('g')
			.data(this.stack.keys(this.data.columns)(this.data));
		// EXIT old elements not present in new data.

		// ENTER new elements present in new data.
		rects
			.enter()
			.append('g')
			.merge(rects)
			.attr('class', 'enter')
			.attr('fill', d => this.vehicleColor(d.key))
			.selectAll('rect')
			.data(d => d)
			.enter()
			.append('rect')
			.attr('x', d => this.x(d.data.vehicleType))
			.attr('y', d => this.y(d[1]))
			.attr('height', d => this.y(d[0]) - this.y(d[1]))
			.attr('width', this.x.bandwidth());

		rects
			.exit()
			.attr('class', 'exit')
			.remove();
	}
}

function type(d, i, columns) {
	let t;
	for (i = 1, t = 0; i < columns.length; ++i)
		t += d[columns[i]] = +d[columns[i]];
	d.total = t;
	return d;
}

new StackAreaChart();
