new (class d3Vis {
	constructor() {
		this.height = 200;
		this.width = 680;
		this._run();
	}
	_run() {
		this.Scaffold();
		this.Scales();
		this.ConstructData();
		this.Operation();
	}
	Scales() {
		this.color = d3
			.scaleOrdinal()
			.domain(["Lemon", "Apple"])
			.range(["yellow", "red"]);
	}
	Scaffold() {
		this.svg = d3
			.select("body")
			.append("svg")
			.attr("width", this.width)
			.attr("height", this.height + 300)
			.append("g")
			.attr("transform", "translate(0," + 150 + ")");
		this.t = d3.transition().duration(800);
	}
	ConstructData() {
		const fruit = (type, id) => {
			return { type, id: id * Math.random() };
		};
		this.makeFruit = n => d3.range(n).map((el, i) => fruit("Apple", i));
	}
	render(selection, { fruit }) {
		this._renderRect(selection);
		this._renderText(selection, fruit);
		this._renderCircle(selection, fruit);
	}
	_renderText(selection, fruit) {
		// dataJoin Constructor
		let texts = selection.selectAll("text").data(fruit, d => d.id);

		// dataJoin enter Manipulation
		texts
			.enter()
			.append("text")
			.attr("class", "text")
			.attr("text-anchor", "middle")
			.attr("x", (d, i) => i * 100 + 120)
			.attr("y", d => this.height - 20)
			.style("fill", d => this.color(d.type))
			.text(d => d.type);

		// should use transition to update the order
		texts
			.transition(this.t)
			.style("fill", d => this.color(d.type))
			.text(d => d.type)
			.attr("x", (d, i) => i * 100 + 120);

		// dataJoin Exit Manipulation
		texts.exit().remove();
	}
	_renderCircle(selection, fruit) {
		// dataJoin Constructor
		let circles = selection.selectAll("circle").data(fruit, d => d.id);

		// dataJoin Exit Manipulation
		circles
			.exit()
			.transition(this.t)
			.attr("r", 0);

		// dataJoin enter Manipulation
		circles
			.enter()
			.append("circle")
			.attr("cy", d => this.height / 2)
			.attr("r", 0)
			.attr("cx", (d, i) => i * 100 + 120)
			.style("fill", d => this.color(d.type))
			.style("stroke", "#262626")
			.transition(this.t)
			.attr("r", (d, i) => i * 10 || 5);

		//dataJoin Update Manipulation
		circles
			.transition(this.t)
			.style("fill", d => this.color(d.type))
			.attr("cx", (d, i) => i * 100 + 120);
	}
	_renderRect(selection) {
		selection
			.selectAll("rect")
			.data([null])
			.enter()
			.append("rect")
			.attr("width", this.width)
			.attr("y", 20)
			.attr("height", this.height)
			.attr("fill", "#ccc")
			.attr("rx", this.height / 2);
	}
	Operation() {
		let fruit = this.makeFruit(5);
		this.render(this.svg, { fruit });

		setTimeout(() => {
			fruit.splice(2, 1);
			this.render(this.svg, { fruit });
		}, 1000);

		setTimeout(() => {
			fruit[3].type = "Lemon";
			this.render(this.svg, { fruit });
		}, 2000);

		setTimeout(() => {
			fruit.push({ type: "Apple", id: Math.random() + 2 });
			this.render(this.svg, { fruit });
		}, 2500);
	}
})();
