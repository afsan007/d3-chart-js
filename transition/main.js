let clamp = (a, b, v) => (v < a ? a : v > b ? b : v)
let width = 710
let height = Math.ceil((width * screen.height) / screen.width)

let rec = {
	width: 20,
	height: 50,
	count: 1000,
	padding: 0,
}

let w = rec.width
let cols = Math.floor(width / w)
let rows = Math.floor(height / w)
let n = cols * rows
let sa = width / cols
let pad = clamp(0.5, w, Math.floor(((w / 2) * rec.padding) / 100))

let rect = d3
	.select('body')
	.append('svg')
	.attr('width', width)
	.attr('height', height)
	.selectAll('rect')
	.data(d3.range(rec.count))

let rectEnter = rect
	.enter()
	.append('rect')
	.attr('x', (d, i) => (i % cols) * sa)
	.attr('y', (d, i) => ((i / cols) | 0) * sa)
	.attr('width', w - pad * 2)
	.attr('height', w - pad * 2)
	.attr('fill', '#eee')
	.on('click', d => {
		let selectRect = d
		let transitionRect = rectEnter
			.transition()
			.delay((d, i) => i + (Math.random() * n) / 5)
			.ease(d3.easeLinear)
			.on('start', function repeat() {
				d3
					.active(this)
					.filter(d => d >= selectRect)
					.styleTween('fill', () => d3.interpolateRgb('#eee', 'green'))
					.transition()
					.delay(1000)
					.styleTween('fill', () => d3.interpolateRgb('green', '#eee'))
					.transition()
					.delay(2000)
					.on('start', repeat)
			})
			.on('end', () => {})
	})

let format = d3.format('.3s')
let random = d3.randomUniform(1e5, 1e7)
let div = d3
	.select('body')
	.append('div')
	.style('font-variant-numeric', 'tabular-nums')
	.property('_current', random)

// div
// 	.transition()
// 	.duration(10000)
// 	.textTween(() => t => `t = ${t.toFixed(6)}`)
// 	.end()

div
	.datum(random)
	.transition()
	.duration(5000)
	.textTween(function (d) {
		const i = d3.interpolate(this._current, d)
		return function (t) {
			return format((this._current = i(t)))
		}
	})
	.end()
