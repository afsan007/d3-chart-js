class Base {
	Scaffold() {
		this.margin = {
			left: 60,
			right: 60,
			top: 50,
			bottom: 100,
		}
		this.height = 500 - this.margin.top - this.margin.bottom
		this.width = 850 - this.margin.left - this.margin.right
		this.g = d3
			.select('#chart-area')
			.append('svg')
			.attr(
				'width',
				this.width + this.margin.left + this.margin.right
			)
			.attr(
				'height',
				this.height + this.margin.top + this.margin.bottom
			)
			.append('g')
			.attr(
				'transform',
				'translate(' +
					this.margin.left +
					', ' +
					this.margin.top +
					')'
			)
	}
}

class ScatterPlots extends Base {
	constructor(props) {
		super(props)
		this.interval
		this.time = 0
		this.formattedData
		this.run()
		this.FetchData()
	}
	run() {
		this.Scaffold()
		this.Labels()
		this.DataNormalize()
		this.Axises()
		this.Tips()
		this.Legend()
		this.UiController()
	}

	Labels() {
		this.xLabel = this.g
			.append('text')
			.attr('y', this.height + 50)
			.attr('x', this.width / 2)
			.attr('font-size', '20px')
			.attr('text-anchor', 'middle')
			.text('GDP Per Capita ($)')
		this.yLabel = this.g
			.append('text')
			.attr('transform', 'rotate(-90)')
			.attr('y', -40)
			.attr('x', -170)
			.attr('font-size', '20px')
			.attr('text-anchor', 'middle')
			.text('Life Expectancy (Years)')
		this.timeLabel = this.g
			.append('text')
			.attr('y', this.height - 10)
			.attr('x', this.width - 40)
			.attr('font-size', '40px')
			.attr('opacity', '0.4')
			.attr('text-anchor', 'middle')
			.text('1800')
	}
	Tips() {
		this.tip = d3
			.tip()
			.attr('class', 'd3-tip')
			.html(d =>
				[
					`<strong>Country:\t</strong><span style='color:red'>${d.country}</span><br><br>`,
					`<strong>Continent:\t</strong><span style='color:red;text-transform:capitalize'>${d.continent}</span><br><br>`,
					`<strong>Life Expectancy:\t</strong><span style='color:red'>${d3.format(
						'.2f'
					)(d.life_exp)}</span><br><br>`,
					`<strong>GDP Per Capita:\t</strong><span style='color:red'>${d3.format(
						'$,.0f'
					)(d.income)}</span><br><br>`,
					`<strong>Population:\t</strong><span style='color:red'>${d3.format(
						',.0f'
					)(d.population)}</span><br>`,
				].join('')
			)
		this.g.call(this.tip)
	}
	Axises() {
		this.xAxisCall = d3
			.axisBottom(this.x)
			.tickValues([400, 4000, 40000])
			.tickFormat(d3.format('$'))
		this.g
			.append('g')
			.attr('class', 'x axis')
			.attr('transform', 'translate(0,' + this.height + ')')
			.call(this.xAxisCall)

		this.yAxisCall = d3.axisLeft(this.y).tickFormat(d => +d)
		this.g
			.append('g')
			.attr('class', 'y axis')
			.call(this.yAxisCall)
	}
	DataNormalize() {
		this.x = d3
			.scaleLog()
			.base(10)
			.range([0, this.width])
			.domain([142, 150000])
		this.y = d3
			.scaleLinear()
			.range([this.height, 0])
			.domain([0, 90])
		this.area = d3
			.scaleLinear()
			.range([25 * Math.PI, 1500 * Math.PI])
			.domain([2000, 1400000000])
		this.continentColor = d3.scaleOrdinal(d3.schemePastel1)
	}
	Legend() {
		this.continents = ['europe', 'asia', 'americas', 'africa']
		// Legend_Box
		this.Legend = this.g
			.append('g')
			.attr(
				'transform',
				'translate(' +
					(this.width - 20) +
					',' +
					(this.height - 160) +
					')'
			)
		this.continents.forEach((item, index) => {
			// legend area
			this.container = this.Legend.append('g').attr(
				'transform',
				'translate(' + 0 + ',' + index * 30 + ')'
			)
			this.container
				.append('rect')
				.attr('width', 20)
				.attr('height', 20)
				.attr('fill', this.continentColor(item))
			this.container
				.append('text')
				.attr('x', -10)
				.attr('y', 15)
				.attr('text-anchor', 'end')
				.style('text-transform', 'capitalize')
				.text(item)
		})
	}
	UiController() {
		let step = () => {
			this.time = this.time < 214 ? this.time + 1 : 0
			this.Update(this.formattedData[this.time])
		}
		$('#play-button').on('click', function () {
			if ($(this).text() === 'play') {
				$(this).text('pause')
				this.interval = setInterval(step, 100)
			} else {
				$(this).text('play')
				clearInterval(this.interval)
			}
		})
		$('#reset-button').on('click', () => {
			this.time = 0
			this.Update(this.formattedData[0])
		})

		$('#continent-select').on('change', e =>
			this.Update(this.formattedData[this.time])
		)
		$('#slider').slider({
			max: 214,
			step: 1,
			slide: (event, ui) => {
				console.log(ui.value)
				this.Update(this.formattedData[ui.value])
				this.time = ui.value
				$('#amount').text(ui.value)
			},
		})
		$('#amount').text($('#slider').slider('value'))
	}
	FetchData() {
		d3.json('data/data.json').then(data => {
			// Clean data
			this.formattedData = data.map(year =>
				year['countries']
					.filter(country => country.income && country.life_exp)
					.map(country => {
						country.income = +country.income
						country.life_exp = +country.life_exp
						return country
					})
			)

			// First run of the visualization
			this.Update(this.formattedData[0])
		})
	}
	Update(data) {
		this.t = d3.transition().duration(100)
		let continent = $('#continent-select').val()
		data = data.filter(d => {
			if (continent === 'all') return true
			return continent === d.continent
		})
		// JOIN new data with old elements.
		this.circles = this.g
			.selectAll('circle')
			.data(data, d => d.country)

		// EXIT old elements not present in new data.
		this.circles.exit().attr('class', 'exit').remove()

		// ENTER new elements present in new data.
		this.circles
			.enter()
			.append('circle')
			.attr('class', 'enter')
			.attr('fill', d => this.continentColor(d.continent))
			.on('mouseover', this.tip.show)
			.on('mouseout', this.tip.hide)
			.merge(this.circles)
			.transition(this.t)
			.attr('cy', d => this.y(d.life_exp))
			.attr('cx', d => this.x(d.income))
			.attr('r', d =>
				Math.sqrt(this.area(d.population) / Math.PI)
			)

		// Update the time label
		this.timeLabel.text(+(this.time + 1800))
		$('#slider').slider('value', this.time)
	}
}

new ScatterPlots()
