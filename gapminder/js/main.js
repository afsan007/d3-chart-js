/*
 *    main.js
 *    Mastering Data Visualization with D3.js
 *    6.2 - Adding a legend
 */

let margin = { left: 60, right: 60, top: 50, bottom: 100 };
let height = 500 - margin.top - margin.bottom,
	width = 850 - margin.left - margin.right;

let g = d3
	.select("#chart-area")
	.append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
let interval;
let time = 0;
let formattedData;
let tip = d3
	.tip()
	.attr("class", "d3-tip")
	.html(d =>
		[
			`<strong>Country:\t</strong><span style='color:red'>${d.country}</span><br><br>`,
			`<strong>Continent:\t</strong><span style='color:red;text-transform:capitalize'>${d.continent}</span><br><br>`,
			`<strong>Life Expectancy:\t</strong><span style='color:red'>${d3.format(
				".2f"
			)(d.life_exp)}</span><br><br>`,
			`<strong>GDP Per Capita:\t</strong><span style='color:red'>${d3.format(
				"$,.0f"
			)(d.income)}</span><br><br>`,
			`<strong>Population:\t</strong><span style='color:red'>${d3.format(",.0f")(
				d.population
			)}</span><br>`,
		].join("")
	);
g.call(tip);
// Scales
let x = d3
	.scaleLog()
	.base(10)
	.range([0, width])
	.domain([142, 150000]);
let y = d3
	.scaleLinear()
	.range([height, 0])
	.domain([0, 90]);
let area = d3
	.scaleLinear()
	.range([25 * Math.PI, 1500 * Math.PI])
	.domain([2000, 1400000000]);
let continentColor = d3.scaleOrdinal(d3.schemePastel1);

// Labels
let xLabel = g
	.append("text")
	.attr("y", height + 50)
	.attr("x", width / 2)
	.attr("font-size", "20px")
	.attr("text-anchor", "middle")
	.text("GDP Per Capita ($)");
let yLabel = g
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", -40)
	.attr("x", -170)
	.attr("font-size", "20px")
	.attr("text-anchor", "middle")
	.text("Life Expectancy (Years)");
let timeLabel = g
	.append("text")
	.attr("y", height - 10)
	.attr("x", width - 40)
	.attr("font-size", "40px")
	.attr("opacity", "0.4")
	.attr("text-anchor", "middle")
	.text("1800");

// X Axis
let xAxisCall = d3
	.axisBottom(x)
	.tickValues([400, 4000, 40000])
	.tickFormat(d3.format("$"));
g.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxisCall);

// Y Axis
let yAxisCall = d3.axisLeft(y).tickFormat(function(d) {
	return +d;
});
g.append("g")
	.attr("class", "y axis")
	.call(yAxisCall);

let continents = ["europe", "asia", "americas", "africa"];
// Legend_Box
let Legend = g
	.append("g")
	.attr("transform", "translate(" + (width - 20) + "," + (height - 160) + ")");
continents.forEach((item, index) => {
	// legend area
	let container = Legend.append("g").attr(
		"transform",
		"translate(" + 0 + "," + index * 30 + ")"
	);
	container
		.append("rect")
		.attr("width", 20)
		.attr("height", 20)
		.attr("fill", continentColor(item));
	container
		.append("text")
		.attr("x", -10)
		.attr("y", 15)
		.attr("text-anchor", "end")
		.style("text-transform", "capitalize")
		.text(item);
});

d3.json("data/data.json").then(function(data) {
	console.log(data);

	// Clean data
	formattedData = data.map(year =>
		year["countries"]
			.filter(country => country.income && country.life_exp)
			.map(country => {
				country.income = +country.income;
				country.life_exp = +country.life_exp;
				return country;
			})
	);

	// First run of the visualization
	update(formattedData[0]);
});

$("#play-button").on("click", function() {
	if ($(this).text() === "play") {
		$(this).text("pause");
		interval = setInterval(() => step(), 100);
	} else {
		$(this).text("play");
		clearInterval(interval);
	}
});
$("#reset-button").on("click", function() {
	time = 0;
	update(formattedData[0]);
});

function step() {
	time = time < 214 ? time + 1 : 0;
	update(formattedData[time]);
}
$("#continent-select").on("change", e => update(formattedData[time]));
$("#slider").slider({
	classes: {
		"ui-widget": "2em",
	},
	value: 0,
	min: 0,
	max: 214,
	step: 1,
	slide: (_event, ui) => {
		update(formattedData[ui.value]);
		time = ui.value;
		$("#amount").text(ui.value);
	},
});
$("#amount").text($("#slider").slider("value"));

function update(data) {
	// Standard transition time for the visualization
	let t = d3.transition().duration(100);
	let continent = $("#continent-select").val();
	data = data.filter(d => {
		if (continent === "all") return true;
		return continent === d.continent;
	});
	// JOIN new data with old elements.
	let circles = g.selectAll("circle").data(data, d => d.country);

	// EXIT old elements not present in new data.
	circles
		.exit()
		.attr("class", "exit")
		.remove();

	// ENTER new elements present in new data.
	circles
		.enter()
		.append("circle")
		.attr("class", "enter")
		.attr("fill", d => continentColor(d.continent))
		.on("mouseover", tip.show)
		.on("mouseout", tip.hide)
		.merge(circles)
		.transition(t)
		.attr("cy", d => y(d.life_exp))
		.attr("cx", d => x(d.income))
		.attr("r", d => Math.sqrt(area(d.population) / Math.PI));

	// Update the time label
	timeLabel.text(+(time + 1800));
	$("#slider").slider("value", this.time);
}
